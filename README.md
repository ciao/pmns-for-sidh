Polynomial Modular Number System for SIDH
=========================================


This repository contains C code implementing PMNS basis for various primes used
in the SIKE submission to the NIST.
The C code implementing the elliptic curve arithmetic and the SIDH and SIKE
functions were taken from
https://github.com/microsoft/PQCrypto-SIDH (commit 4eb51ae0bc).


How to compile
==============

To compile the code with the our version of the arithmetic using PMNS:
```
make OPT_LEVEL=PMNS
```
For primes for which more than one implementation exist, one can select the
variant, for example for the prime P503 and the variant 3x3:
```
make OPT_LEVEL=PMNS PMNS_P503_VARIANT=3x3
```

To compile the code with the optimized implementation of the arithmetic from the
PQCrypto-SIDH repository:
```
make OPT_LEVEL=FAST
```


Differences with the reference implementation
=============================================

A diff between our code and the reference implementation can be obtain by
executing the following commands in a directory outside of the current
repository tree:
```
git clone https://github.com/microsoft/PQCrypto-SIDH
cd PQCrypto-SIDH
git checkout 4eb51ae0bc
diff -r -x .git . /path/to/this/repo/
```
