#include "../src/P503/P503_internal.h"

#define from_PMNS from_PMNS_503
#define to_PMNS to_PMNS_503
#define fpadd fpadd503
#define fpsub fpsub503
#define fpneg fpneg503
#define fpdiv2 fpdiv2_503
#define fpmul_mont fpmul503_mont
#define fpinv_mont fpinv503_mont
#define fp2add fp2add503
#define fp2sub fp2sub503
#define fp2mul_mont fp2mul503_mont
#define fp2sqr_mont fp2sqr503_mont
#define fp2inv_mont fp2inv503_mont
#define mp_dblsubx2_pmns mp_dblsub503x2_pmns
#define mp_subx2_pmns mp_sub503x2_pmns

#include "arith_benchs.c"
