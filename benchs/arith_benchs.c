#include <stdio.h>
#include <inttypes.h>

#include "../src/config.h"
#include "../tests/test_extras.h"

#define BENCHMARK_INTERNALS

static void
fp2random_pmns_test (f2elm_t r)
{
  fprandom_pmns_test (r[0]);
  fprandom_pmns_test (r[1]);
}


void convert_run()
{
  uint64_t cycles, nbenchs;
  felm_t a, c;

  printf("\n--------------------------------------------------------------------------------------------------------\n\n");
  printf ("Benchmarking conversion from/to PMNS:\n\n");

  /* from_PMNS */
  fprandom_pmns_test(a);
  cycles = 0;
  nbenchs = (1UL << 22);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    from_PMNS (a, c);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  from_PMNS ......................... %7" PRIu64 " cycles\n", cycles/nbenchs);

  /* to_PMNS */
  fprandom_pmns_test(c);
  cycles = 0;
  nbenchs = (1UL << 22);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    to_PMNS (c, a);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  to_PMNS ........................... %7" PRIu64 " cycles\n", cycles/nbenchs);
}

void fp_run()
{
  uint64_t cycles, nbenchs;
  felm_t a, b, c;
  digit_t a2[2*NWORDS_FIELD];

  printf("\n--------------------------------------------------------------------------------------------------------\n\n");
  printf ("Benchmarking PMNS field arithmetic over GF(p):\n\n");

  /* GF(p) add */
  fprandom_pmns_test(a); fprandom_pmns_test(b);
  cycles = 0;
  nbenchs = (1UL << 24);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fpadd (a, b, c);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p) addition runs in ........................................... %7" PRIu64 " cycles\n", cycles/nbenchs);


  /* GF(p) sub */
  fprandom_pmns_test(a); fprandom_pmns_test(b);
  cycles = 0;
  nbenchs = (1UL << 24);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fpsub (a, b, c);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p) substraction runs in ....................................... %7" PRIu64 " cycles\n", cycles/nbenchs);


  /* GF(p) mul */
  fprandom_pmns_test(a); fprandom_pmns_test(b);
  cycles = 0;
  nbenchs = (1UL << 23);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fpmul_mont (a, b, c);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p) multiplication runs in ..................................... %7" PRIu64 " cycles\n", cycles/nbenchs);

  /* GF(p) reduction */
  fprandomx2_pmns_test(a2);
  cycles = 0;
  nbenchs = (1UL << 24);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    rdc_mont (a2, a);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p) reduction runs in .......................................... %7" PRIu64 " cycles\n", cycles/nbenchs);

  /* GF(p) inversion */
  fprandom_pmns_test(a);
  cycles = 0;
  nbenchs = (1UL << 14);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fpinv_mont (a);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p) inversion (exponentiation) runs in ......................... %7" PRIu64 " cycles\n", cycles/nbenchs);

#ifdef BENCHMARK_INTERNALS

  digit_t b2[2*NWORDS_FIELD], c2[2*NWORDS_FIELD];;

  printf ("\n    Internal functions:\n\n");

  /* fpneg */
  fprandom_pmns_test(a);
  cycles = 0;
  nbenchs = (1UL << 24);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fpneg (a);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("    fpneg runs in .................. %7" PRIu64 " cycles\n", cycles/nbenchs);


  /* fpdiv2 */
  fprandom_pmns_test(a);
  cycles = 0;
  nbenchs = (1UL << 24);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fpdiv2 (a, c);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("    fpdiv2 runs in ................ %7" PRIu64 " cycles\n", cycles/nbenchs);


  /* mp_dblsubx2_pmns */
  fprandomx2_pmns_test(a2); fprandomx2_pmns_test(b2);
  cycles = 0;
  nbenchs = (1UL << 24);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    mp_dblsubx2_pmns (a2, b2, c2);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("    mp_dblsubx2_pmns runs in ....... %7" PRIu64 " cycles\n", cycles/nbenchs);


  /* mp_subx2_pmns */
  fprandomx2_pmns_test(a2); fprandomx2_pmns_test(b2);
  cycles = 0;
  nbenchs = (1UL << 24);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    mp_subx2_pmns (a2, b2, c2);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("    mp_subx2_pmns runs in .......... %7" PRIu64 " cycles\n", cycles/nbenchs);


  /* mp_add */
  fprandom_pmns_test(a); fprandom_pmns_test(b);
  cycles = 0;
  nbenchs = (1UL << 23);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    mp_add (a, b, c, NCOEFFS_FIELD);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("    mp_add ............................ %7" PRIu64 " cycles\n", cycles/nbenchs);

  /* mp_mul */
  fprandom_pmns_test(a); fprandom_pmns_test(b);
  cycles = 0;
  nbenchs = (1UL << 23);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    mp_mul (a, b, c2, NWORDS_FIELD);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("    mp_mul ............................ %7" PRIu64 " cycles\n", cycles/nbenchs);
#endif
}

void fp2_run()
{
  uint64_t cycles, nbenchs;
  f2elm_t p, q, r;

  printf("\n--------------------------------------------------------------------------------------------------------\n\n");
  printf ("Benchmarking PMNS quadratic extension arithmetic over GF(p^2):\n\n");


  /* GF(p^2) add */
  fp2random_pmns_test(p); fp2random_pmns_test(q);
  cycles = 0;
  nbenchs = (1UL << 24);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fp2add (p, q, r);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p^2) addition runs in ......................................... %7" PRIu64 " cycles\n", cycles/nbenchs);

  /* GF(p^2) sub */
  fp2random_pmns_test(p); fp2random_pmns_test(q);
  cycles = 0;
  nbenchs = (1UL << 24);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fp2sub (p, q, r);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p^2) substraction runs in ..................................... %7" PRIu64 " cycles\n", cycles/nbenchs);

  /* GF(p^2) mul */
  fp2random_pmns_test(p); fp2random_pmns_test(q);
  cycles = 0;
  nbenchs = (1UL << 23);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fp2mul_mont (p, q, r);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p^2) multiplication runs in ................................... %7" PRIu64 " cycles\n", cycles/nbenchs);


  /* GF(p^2) sqr */
  fp2random_pmns_test(p);
  cycles = 0;
  nbenchs = (1UL << 23);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fp2sqr_mont (p, r);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p^2) squaring runs in ......................................... %7" PRIu64 " cycles\n", cycles/nbenchs);

  /* GF(p^2) inversion */
  fp2random_pmns_test(p);
  cycles = 0;
  nbenchs = (1UL << 14);
  for (unsigned int n = 0; n < nbenchs; n++)
  {
    uint64_t cycles_start = cpucycles();

    fp2inv_mont (p);

    uint64_t cycles_end = cpucycles();
    cycles += cycles_end-cycles_start;
  }
  printf ("  GF(p^2) inversion (exponentiation) runs in ....................... %7" PRIu64 " cycles\n", cycles/nbenchs);
}

void ecisog_run()
{
    int n;
    unsigned long long cycles, cycles1, cycles2, nbenchs;
    unsigned long long _dbl, _tpl, _4iso_pt, _4iso_eval, _3iso_pt, _3iso_eval;
    f2elm_t A24, C24, A4, A, C, coeff[5];
    point_proj_t P, Q;

    printf("\n--------------------------------------------------------------------------------------------------------\n\n");
    printf("Benchmarking PMNS elliptic curve and isogeny functions: \n\n");

    // Point doubling
    cycles = 0;
    nbenchs = (1UL << 17);
    for (n=0; n<nbenchs; n++)
    {
      fp2random_pmns_test (A24); fp2random_pmns_test (C24);
      fp2random_pmns_test (P->X); fp2random_pmns_test (P->Z);

      cycles1 = cpucycles();
      xDBL(P, Q, A24, C24);
      cycles2 = cpucycles();
      cycles = cycles+(cycles2-cycles1);
    }
    _dbl = cycles/nbenchs;
    printf("  Point doubling runs in .......................................... %7lld ", _dbl); print_unit;
    printf("\n");

    // 4-isogeny of a projective point
    cycles = 0;
    nbenchs = (1UL << 17);
    for (n=0; n<nbenchs; n++)
    {
      fp2random_pmns_test (P->X); fp2random_pmns_test (P->Z);

      cycles1 = cpucycles();
      get_4_isog(P, A, C, coeff);
      cycles2 = cpucycles();
      cycles = cycles+(cycles2-cycles1);
    }
    _4iso_pt = cycles/nbenchs;
    printf("  4-isogeny of projective point runs in ........................... %7lld ", _4iso_pt); print_unit;
    printf("\n");

    // 4-isogeny evaluation at projective point
    cycles = 0;
    nbenchs = (1UL << 17);
    for (n=0; n<nbenchs; n++)
    {
      fp2random_pmns_test (P->X); fp2random_pmns_test (P->Z);
      for (unsigned int i = 0; i < 3; i++)
        fp2random_pmns_test (coeff[i]);

      cycles1 = cpucycles();
      eval_4_isog(P, coeff);
      cycles2 = cpucycles();
      cycles = cycles+(cycles2-cycles1);
    }
    _4iso_eval = cycles/nbenchs;
    printf("  4-isogeny evaluation at projective point runs in ................ %7lld ", _4iso_eval); print_unit;
    printf("\n");
    printf("  Point quadrupling (point doubling x2) / 4-isogeny ratio ......... %9.4f ", 2.0*_dbl/(_4iso_pt+_4iso_eval));
    printf("\n");

    // Point tripling
    cycles = 0;
    nbenchs = (1UL << 17);
    for (n=0; n<nbenchs; n++)
    {
      fp2random_pmns_test (A4); fp2random_pmns_test (C);
      fp2random_pmns_test (P->X); fp2random_pmns_test (P->Z);

      cycles1 = cpucycles();
      xTPL(P, Q, A4, C);
      cycles2 = cpucycles();
      cycles = cycles+(cycles2-cycles1);
    }
    _tpl = cycles/nbenchs;
    printf("  Point tripling runs in .......................................... %7lld ", _tpl); print_unit;
    printf("\n");

    // 3-isogeny of a projective point
    cycles = 0;
    nbenchs = (1UL << 17);
    for (n=0; n<nbenchs; n++)
    {
      fp2random_pmns_test (P->X); fp2random_pmns_test (P->Z);

      cycles1 = cpucycles();
      get_3_isog(P, A, C, coeff);
      cycles2 = cpucycles();
      cycles = cycles+(cycles2-cycles1);
    }
    _3iso_pt = cycles/nbenchs;
    printf("  3-isogeny of projective point runs in ........................... %7lld ", _3iso_pt); print_unit;
    printf("\n");

    // 3-isogeny evaluation at projective point
    cycles = 0;
    nbenchs = (1UL << 17);
    for (n=0; n<nbenchs; n++)
    {
      fp2random_pmns_test (Q->X); fp2random_pmns_test (Q->Z);
      for (unsigned int i = 0; i < 2; i++)
        fp2random_pmns_test (coeff[i]);

      cycles1 = cpucycles();
      eval_3_isog(Q, coeff);
      cycles2 = cpucycles();
      cycles = cycles+(cycles2-cycles1);
  }
    _3iso_eval = cycles/nbenchs;
    printf("  3-isogeny evaluation at projective point runs in ................ %7lld ", _3iso_eval); print_unit;
    printf("\n");
    printf("  Point tripling / 3-isogeny ratio ................................ %9.4f ", (1.0*_tpl)/(_3iso_pt+_3iso_eval));
    printf("\n");
    
}


int main (int argc, char *argv[])
{

  convert_run();
  fp_run();                // Benchmark field operations using p
  fp2_run();               // Benchmark arithmetic functions over GF(p^2)
  ecisog_run();            // Benchmark elliptic curve and isogeny functions

  return 0;
}

