#ifndef PMNS_CONSTANTS_H
#define PMNS_CONSTANTS_H

/* Alice's generator values {XPA0 + XPA1*i, XQA0 + XQA1*i, XRA0 + XRA1*i}
 * in GF(p^2), expressed in PMNS--Montgomery representation.
 */
static const uint64_t A_gen[6*NWORDS_FIELD] = { 0x0003b78a1dd5e9ae,
            0x00030bc511563885, 0x0000b67f12595d95, 0x00041d4929b52487,
            0x0003a3fec8b4b428, 0x00008f11975b73ad, 0x000479d3ca7017a2,
            0x0001b13e1ba7fdf8, 0x000291e1e4cb1019, 0x0000dbaa286e01ff, //XPA0
                                                0x00031809459f2e0b,
            0x0004a74b7c3fc07a, 0x0003864385443e8b, 0x0000177fc1e7aa3f,
            0x000482e1811cd5f6, 0x0004fe92f53062e1, 0x00032704223d924c,
            0x0000787f8ee55356, 0x000447e5d953284c, 0x0000452f756b0dc5, //XPA1
                                                0x00018707feb5d324,
            0x00025e29985f671d, 0x0001235091b97c87, 0x0004e43fe56e3e4b,
            0x000118ead9dd4dc0, 0x0001e2adf97389c0, 0x00032860e28939b0,
            0x000364b3b26d2e72, 0x0002ac52bc2b8b17, 0x00000471e66af113, //XQA0
                                                0x0004f4f891ad6b7d,
            0x00011721377809ab, 0x00003ddecdbf0f40, 0x0004b9a223a72fdd,
            0x0001dfe48aed01d9, 0x00046575f4bfe867, 0x000308032305ecc1,
            0x00034c352e3c0711, 0x00011ab7d9e26770, 0x00014bd3919eb435, //XQA1
                                                0x00005f6416b79259,
            0x0001eb880207e4fa, 0x00013fdba1ab78d0, 0x0000beb27fa99142,
            0x000148576b14cead, 0x0000041ee12614b9, 0x000250424bef6ee8,
            0x000194c1508c1859, 0x00017042215bede6, 0x0001230979a821f5, //XRA0
                                                0x0000abf22f7b619e,
            0x000336f5eb35b8e2, 0x00015d14d554faaf, 0x00026a5900859a50,
            0x00013abd66ac3986, 0x0002071e7bff77ee, 0x00013af1c4d6260d,
            0x0004c8a10e161a69, 0x0003786c6abc5ef0, 0x0001afdc36b8190b }; //XRA1

/* Bob's generator values {XPB0, XQB0, XRB0 + XRB1*i} in GF(p^2),
 * expressed in PMNS--Montgomery representation.
 */
static const uint64_t B_gen[6*NWORDS_FIELD] = { 0x000248bca9fca4e3,
            0x000226cb31ba458b, 0x000513536fdae5dc, 0x00034a9ab3dd121f,
            0x0001cfa5c8d5b154, 0x00050845859508be, 0x0001b776bb860c78,
            0x0003b680b2abc894, 0x0002fc9a9c950880, 0x0000797cfbc161ae, //XPB0
                                                0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000, //XPB1
                                                0x00048b7213cdf835,
            0x000470420790b402, 0x00006fb2c4d13049, 0x000294faee1df82a,
            0x0002dd9d7114de1f, 0x00048e4cd77a5444, 0x000027c5dd100b46,
            0x000239494c8f8c02, 0x00011a98c3feda05, 0x00013db6fcfca31c, //XQB0
                                                0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000, //XQB1
                                                0x000491fe92929ac7,
            0x0002921983a31c5a, 0x000125690878d2b7, 0x0001b43645fb5b04,
            0x00046a909e926bee, 0x000009431f19a040, 0x0004b3348747f386,
            0x0002073d37a31563, 0x0001b5cbfbd8f745, 0x00009c0aa7d37643, //XRB0
                                                0x000244125901fab9,
            0x000076b86e8d7816, 0x00002662ca0ffc73, 0x00012e5ad808ad41,
            0x00022f60b97b6180, 0x0001effc09b8a873, 0x0002f771b49ab35b,
            0x0004d6b07b680817, 0x0003aeeae06cbb0e, 0x000122ce7f63fd09 }; //XRB1

/* Montgomery constant Montgomery_R2 = (2^64)^2 in PMNS, i.e.,
 * 2^64 in PMNS--Montgomery representation
 */
static const uint64_t Montgomery_R2[NWORDS_FIELD] = { 0x0004416cca000000,
            0x00012123f2eae5db, 0x0000000009b8bd84, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000 };

/* Value one in PMNS--Montgomery representation */
static const uint64_t Montgomery_one[NWORDS_FIELD] = { 0x0000bb60ba000000,
            0x00000000000031e3, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000 };

#endif /* PMNS_CONSTANTS_H */
