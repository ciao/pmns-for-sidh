#include "../../P503_internal.h"

#define KARATSUBA /* use KARATSUBA in mp_mul, if assembly version is not used */

#define fpcopy                        fpcopy503
#define fpadd                         fpadd503
#define fpsub                         fpsub503
#define fpneg                         fpneg503
#define fpdiv2                        fpdiv2_503
#define fpcorrection                  fpcorrection503
#define p                             p503
#define from_PMNS                     from_PMNS_503
#define to_PMNS                       to_PMNS_503
#define mp_dblsubx2_pmns              mp_dblsub503x2_pmns
#define mp_subx2_pmns                 mp_sub503x2_pmns

#define _NO_FPADD /* implemented in assembly */
#define _NO_FPSUB /* implemented in assembly */
#define _NO_FPDIV2 /* implemented in assembly */
#define _NO_MP_MUL /* implemented in assembly */
#define _NO_RDC_MONT /* implemented in assembly */
#define _NO_MP_DBLSUBx2_PMNS /* implemented in assembly */
#define _NO_MP_SUBx2_PMNS /* implemented in assembly */
#define _NO_MP_ADD /* implemented in assembly */

#include "fp_pmns_generic.c"
