#ifndef PMNS_CONSTANTS_H
#define PMNS_CONSTANTS_H

/* Alice's generator values {XPA0 + XPA1*i, XQA0 + XQA1*i, XRA0 + XRA1*i}
 * in GF(p^2), expressed in PMNS--Montgomery representation.
 */
static const uint64_t A_gen[6*NWORDS_FIELD] = { 0x40115b0a129e7d0b,
            0x3f0074b82aada21d, 0x000000aef7c1ddfd, 0x634ec26a2e9e70f4,
            0xad805f318922ed2d, 0x00000026f7176b5a, 0xbbbc135c16a8c127,
            0x6dcba858fefb6264, 0x00000023a27a69b8, //XPA0
                                                0xae3b5f662cbc9074,
            0x702e78ccaa9178e8, 0x000000cc290c1167, 0xa27902bb02b5c7f7,
            0x50f22392158c52ee, 0x000000df3cc4aafe, 0x4628904c17b4afe6,
            0x8d148373111806cb, 0x00000018df3fcd3d, //XPA1
                                                0xf433d6c16372f676,
            0xdda3405fd14d9a6c, 0x0000007e3242b3e0, 0x3808f235163e76c5,
            0x528ec53463be05f6, 0x000000020390a53b, 0xc149935665abf5f2,
            0x6b15f6f5e995cc03, 0x00000030539cca09, //XQA0
                                                0x0cedda18f62dd795,
            0xf4d76b91f9400141, 0x0000007e5676df93, 0xb0865ba6bc60864b,
            0xcbc0ea62a0dc25e1, 0x00000082d20bb46d, 0xd96a2d3db7789b0a,
            0x72613c0b2d22d23f, 0x00000031c9a88ff7, //XQA1
                                                0xfb1f34f40d7a06c1,
            0x6e3d0bc80d98f16c, 0x0000003bd31be0f9, 0x4f31db23b25049bc,
            0xc69dbf954a4bd385, 0x000000acf6e96a24, 0x26b6a22e64c50efe,
            0xd1baa326a23baa86, 0x0000002789f65b6c, //XRA0
                                                0x3b110aa193d5418d,
            0x51893500f0adcdc7, 0x000000b5048ded97, 0xb91e44bd27e48c8b,
            0x417c4ea66ab31de5, 0x000000b1945d57d6, 0x3e14bb40225c5108,
            0x0e23852673186215, 0x00000023b53b4964 }; //XRA1

/* Bob's generator values {XPB0, XQB0, XRB0 + XRB1*i} in GF(p^2),
 * expressed in PMNS--Montgomery representation.
 */
static const uint64_t B_gen[6*NWORDS_FIELD] = { 0xce8cf51907c8db92,
            0x33ccb3527be343a2, 0x0000008c84d32ecc, 0x7772358ec1f05967,
            0x734a865a330fdca6, 0x000000abf7b03cf1, 0xeeb7bf8c3d282a6c,
            0x5d32099f9189d00c, 0x0000003a0cd7aae7, //XPB0
                                                0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, //XPB1
                                                0x300ea80231b5ea78,
            0x611bc921c1f2820f, 0x00000060e7663cc6, 0xabf012ebe83c4bcc,
            0x18b384d11626f34c, 0x000000acc41fe7c6, 0x19931ef6edae4efa,
            0xc1469ce0d4d7f640, 0x0000000478c2e894, //XQB0
                                                0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, //XQB1
                                                0x43454f72306686bc,
            0x92eaf41f4de163e5, 0x000000543b10cb43, 0x27554ce8fc12f8b6,
            0x769c2e9c08a21299, 0x000000859d2e53fd, 0x30a8ee58477a600e,
            0x29fe1d9de9bcf978, 0x0000000937c7c35d, //XRB0
                                                0x81241f852e7c6207,
            0x13f9a92ce4084be1, 0x0000001665716ff4, 0xc4c322c998a2f8d9,
            0xe77d31acf59e5ba9, 0x0000003f24fd168f, 0xa8beee523072e51a,
            0xf99d7e69b449ea99, 0x00000016cb0d205c }; //XRB1

/* Montgomery constant Montgomery_R2 = (2^192)^2 in PMNS, i.e.,
 * 2^192 in PMNS--Montgomery representation
 */
static const uint64_t Montgomery_R2[NWORDS_FIELD] = { 0x0000000000000000,
            0xb48539e4e3400000, 0x0000000fda3ef2b7, 0xa9a487eb17810304,
            0xc7b1b6726732da39, 0x0000001d63ca6dea, 0x0000feeee0021022,
            0x0000000000000000, 0x0000000000000000 };

/* Value one in PMNS--Montgomery representation */
static const uint64_t Montgomery_one[NWORDS_FIELD] = { 0x0000000000000000,
            0xf19fe627a0f00000, 0x0000008129e27dd6, 0x0000000000ff774b,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000 };

#endif /* PMNS_CONSTANTS_H */
