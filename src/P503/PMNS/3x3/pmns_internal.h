#ifndef PMNS_INTERNAL_H
#define PMNS_INTERNAL_H

#define NCOEFFS_FIELD 3 /* Number of PMNS coeffs */
#define NWORDS_COEFF 3  /* Number of words of one PMNS coeff */
#define PMNS_E 4
/* Upper bound on the coefficient size:
 *    a coeff c is said to be reduced iif c < 2^K
 */
#define PMNS_K 170
#define PMNS_K_last_digit_shift 42
#define PMNS_K_last_digit_mask 0x000003ffffffffff

#endif /* PMNS_INTERNAL_H */
