#include "P736_api.h"
#include "P736_internal.h"

const uint64_t p736[NWORDS64_FIELD] = { 0xffffffffffffffff,
            0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff,
            0xffffffffffffffff, 0xab72e1ffffffffff, 0x808ce834d8521ec7,
            0x68d8b7360d286384, 0x3809851ac7978376, 0x9ceb48c0d689e91f,
            0x5dc26f7e4c973891, 0x00000000849e9def };

// Order of Alice's subgroup
const uint64_t Alice_order[NWORDS64_ORDER] = { 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000010000000000 };
// Order of Bob's subgroup
const uint64_t Bob_order[NWORDS64_ORDER] = { 0x1a6c290f63d5b971,
            0x9b069431c2404674, 0x8d63cbc1bb346c5b, 0x606b44f48f9c04c2,
            0xbf264b9c48ce75a4, 0x00424f4ef7aee137 };

#ifdef PMNS_IMPLEMENTATION
  #include "pmns_constants.h"
#endif

const unsigned int strat_Alice[MAX_Alice-1] = { 82, 44, 27, 12, 7, 4, 2, 1, 1, 2, 1, 1, 3, 2, 1, 1, 1, 1, 5, 3, 2, 1, 1, 1, 1, 2, 1, 1, 1, 12, 7, 4, 2, 1, 1, 2, 1, 1, 3, 2, 1, 1, 1, 1, 5, 3, 2, 1, 1, 1, 1, 2, 1, 1, 1, 17, 12, 7, 4, 2, 1, 1, 2, 1, 1, 3, 2, 1, 1, 1, 1, 5, 3, 2, 1, 1, 1, 1, 2, 1, 1, 1, 8, 4, 2, 1, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 35, 20, 12, 7, 4, 2, 1, 1, 2, 1, 1, 3, 2, 1, 1, 1, 1, 5, 3, 2, 1, 1, 1, 1, 2, 1, 1, 1, 8, 5, 3, 2, 1, 1, 1, 1, 2, 1, 1, 1, 4, 2, 1, 1, 2, 1, 1, 16, 9, 4, 2, 2, 1, 1, 1, 2, 1, 1, 4, 2, 1, 1, 1, 2, 1, 1, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1 };
const unsigned int strat_Bob[MAX_Bob-1] = { 109, 63, 32, 16, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 16, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 31, 16, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 15, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 7, 4, 2, 1, 1, 2, 1, 1, 3, 2, 1, 1, 1, 1, 51, 27, 15, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 7, 4, 2, 1, 1, 2, 1, 1, 3, 2, 1, 1, 1, 1, 12, 7, 4, 2, 1, 1, 2, 1, 1, 3, 2, 1, 1, 1, 1, 5, 3, 2, 1, 1, 1, 1, 2, 1, 1, 1, 23, 12, 8, 4, 2, 1, 1, 2, 1, 1, 4, 2, 1, 1, 2, 1, 1, 5, 3, 2, 1, 1, 1, 1, 2, 1, 1, 1, 9, 7, 3, 2, 1, 1, 1, 1, 3, 2, 1, 1, 1, 1, 4, 2, 1, 1, 1, 2, 1, 1 };

// Setting up macro defines and including GF(p), GF(p^2), curve, isogeny and kex functions
#define fpcopy                        fpcopy736
#define fpzero                        fpzero736
#define fpadd                         fpadd736
#define fpsub                         fpsub736
#define fpneg                         fpneg736
#define fpdiv2                        fpdiv2_736
#define fpcorrection                  fpcorrection736
#define fpmul_mont                    fpmul736_mont
#define fpsqr_mont                    fpsqr736_mont
#define fpinv_mont                    fpinv736_mont
#define fpinv_chain_mont              fpinv736_chain_mont
#define fpinv_mont_bingcd             fpinv736_mont_bingcd
#define fp2copy                       fp2copy736
#define fp2zero                       fp2zero736
#define fp2add                        fp2add736
#define fp2sub                        fp2sub736
#define mp_sub_p2                     mp_sub736_p2
#define mp_sub_p4                     mp_sub736_p4
#define sub_p4                        mp_sub_p4
#define fp2neg                        fp2neg736
#define fp2div2                       fp2div2_736
#define fp2correction                 fp2correction736
#define fp2mul_mont                   fp2mul736_mont
#define fp2sqr_mont                   fp2sqr736_mont
#define fp2inv_mont                   fp2inv736_mont
#define fp2inv_mont_bingcd            fp2inv736_mont_bingcd
#define fpequal_non_constant_time     fpequal736_non_constant_time
#define mp_add_asm                    mp_add736_asm
#define mp_subaddx2_asm               mp_subadd736x2_asm
#define mp_dblsubx2_asm               mp_dblsub736x2_asm
#define crypto_kem_keypair            crypto_kem_keypair_SIKEp736
#define crypto_kem_enc                crypto_kem_enc_SIKEp736
#define crypto_kem_dec                crypto_kem_dec_SIKEp736
#define random_mod_order_A            random_mod_order_A_SIDHp736
#define random_mod_order_B            random_mod_order_B_SIDHp736
#define EphemeralKeyGeneration_A      EphemeralKeyGeneration_A_SIDHp736
#define EphemeralKeyGeneration_B      EphemeralKeyGeneration_B_SIDHp736
#define EphemeralSecretAgreement_A    EphemeralSecretAgreement_A_SIDHp736
#define EphemeralSecretAgreement_B    EphemeralSecretAgreement_B_SIDHp736

#define from_PMNS                     from_PMNS_736
#define to_PMNS                       to_PMNS_736
#define mp_subx2_pmns                 mp_sub736x2_pmns
#define mp_dblsubx2_pmns              mp_dblsub736x2_pmns

#include "../fpx.c"
#include "../ec_isogeny.c"
#include "../sidh.c"
#include "../sike.c"
