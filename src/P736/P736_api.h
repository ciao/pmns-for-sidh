
#ifndef P736_API_H
#define P736_API_H

/*********************** Key encapsulation mechanism API ***********************/

#define CRYPTO_SECRETKEYBYTES     631    // MSG_BYTES + SECRETKEY_B_BYTES + CRYPTO_PUBLICKEYBYTES bytes
#define CRYPTO_PUBLICKEYBYTES     552
#define CRYPTO_BYTES               32
#define CRYPTO_CIPHERTEXTBYTES    584    // CRYPTO_PUBLICKEYBYTES + MSG_BYTES bytes

// Algorithm name
#define CRYPTO_ALGNAME "SIKEp736"

// SIKE's key generation
// It produces a private key sk and computes the public key pk.
int crypto_kem_keypair_SIKEp736(unsigned char *pk, unsigned char *sk);

// SIKE's encapsulation
int crypto_kem_enc_SIKEp736(unsigned char *ct, unsigned char *ss, const unsigned char *pk);

// SIKE's decapsulation
int crypto_kem_dec_SIKEp736(unsigned char *ss, const unsigned char *ct, const unsigned char *sk);


/*********************** Ephemeral key exchange API ***********************/

#define SIDH_SECRETKEYBYTES_A    45
#define SIDH_SECRETKEYBYTES_B    47
#define SIDH_PUBLICKEYBYTES     552
#define SIDH_BYTES              184

// SECURITY NOTE: SIDH supports ephemeral Diffie-Hellman key exchange. It is NOT secure to use it with static keys.
// See "On the Security of Supersingular Isogeny Cryptosystems", S.D. Galbraith, C. Petit, B. Shani and Y.B. Ti, in ASIACRYPT 2016, 2016.
// Extended version available at: http://eprint.iacr.org/2016/859

// Generation of Alice's secret key
void random_mod_order_A_SIDHp736(unsigned char* random_digits);

// Generation of Bob's secret key
void random_mod_order_B_SIDHp736(unsigned char* random_digits);

// Alice's ephemeral public key generation
int EphemeralKeyGeneration_A_SIDHp736(const unsigned char* PrivateKeyA, unsigned char* PublicKeyA);

// Bob's ephemeral key-pair generation
int EphemeralKeyGeneration_B_SIDHp736(const unsigned char* PrivateKeyB, unsigned char* PublicKeyB);

// Alice's ephemeral shared secret computation
// It produces a shared secret key SharedSecretA using her secret key PrivateKeyA and Bob's public key PublicKeyB
int EphemeralSecretAgreement_A_SIDHp736(const unsigned char* PrivateKeyA, const unsigned char* PublicKeyB, unsigned char* SharedSecretA);

// Bob's ephemeral shared secret computation
// It produces a shared secret key SharedSecretB using his secret key PrivateKeyB and Alice's public key PublicKeyA
int EphemeralSecretAgreement_B_SIDHp736(const unsigned char* PrivateKeyB, const unsigned char* PublicKeyA, unsigned char* SharedSecretB);

#endif
