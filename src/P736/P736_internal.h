#ifndef P736_INTERNAL_H
#define P736_INTERNAL_H

#include "../config.h"

#ifdef PMNS_IMPLEMENTATION
  #include "pmns_internal.h"
  #define NWORDS_FIELD    (NCOEFFS_FIELD*NWORDS_COEFF)
#endif

// Basic constants

#define NBITS_FIELD             736
#define MAXBITS_FIELD           768
#define MAXWORDS_FIELD          ((MAXBITS_FIELD+RADIX-1)/RADIX)     // Max. number of words to represent field elements
#define NWORDS64_FIELD          ((NBITS_FIELD+63)/64)               // Number of 64-bit words of a 736-bit field element
#define NBITS_ORDER             384
#define NWORDS_ORDER            ((NBITS_ORDER+RADIX-1)/RADIX)       // Number of words of oA and oB, where oA and oB are the subgroup orders of Alice and Bob, resp.
#define NWORDS64_ORDER          ((NBITS_ORDER+63)/64)               // Number of 64-bit words of a 256-bit element
#define MAXBITS_ORDER           NBITS_ORDER
#define ALICE                   0
#define BOB                     1
#define OALICE_BITS             360
#define OBOB_BITS               375
#define OBOB_EXPON              236
#define MASK_ALICE              0xff
#define MASK_BOB                0x3f
#define PRIME                   p736
#define PARAM_A                 6
#define PARAM_C                 1
// Fixed parameters for isogeny tree computation
#define MAX_INT_POINTS_ALICE    8
#define MAX_INT_POINTS_BOB      8
#define MAX_Alice               180
#define MAX_Bob                 236
#define MSG_BYTES               32
#define SECRETKEY_A_BYTES       ((OALICE_BITS + 7) / 8)
#define SECRETKEY_B_BYTES       ((OBOB_BITS - 1 + 7) / 8)
#define FP2_ENCODED_BYTES       2*((NBITS_FIELD + 7) / 8)

// SIDH's basic element definitions and point representations

typedef digit_t felm_t[NWORDS_FIELD];                                 // Datatype for representing 736-bit field elements (768-bit max.)
typedef digit_t dfelm_t[2*NWORDS_FIELD];                              // Datatype for representing double-precision 2x736-bit field elements (2x768-bit max.)
typedef felm_t  f2elm_t[2];                                           // Datatype for representing quadratic extension field elements GF(p736^2)

typedef struct { f2elm_t X; f2elm_t Z; } point_proj;                  // Point representation in projective XZ Montgomery coordinates.
typedef point_proj point_proj_t[1];

#ifdef COMPRESS
    typedef struct { f2elm_t X; f2elm_t Y; f2elm_t Z; } point_full_proj;  // Point representation in full projective XYZ Montgomery coordinates
    typedef point_full_proj point_full_proj_t[1];

    typedef struct { f2elm_t x; f2elm_t y; } point_affine;                // Point representation in affine coordinates.
    typedef point_affine point_t[1];

    typedef f2elm_t publickey_t[3];
#endif



/**************** Function prototypes ****************/
/************* Multiprecision functions **************/

// Copy wordsize digits, c = a, where lng(a) = nwords
void copy_words(const digit_t* a, digit_t* c, const unsigned int nwords);

// Multiprecision addition, c = a+b, where lng(a) = lng(b) = nwords. Returns the carry bit
unsigned int mp_add(const digit_t* a, const digit_t* b, digit_t* c, const unsigned int nwords);

// 736-bit multiprecision addition, c = a+b
void mp_add736(const digit_t* a, const digit_t* b, digit_t* c);
void mp_add736_asm(const digit_t* a, const digit_t* b, digit_t* c);

// Multiprecision subtraction, c = a-b, where lng(a) = lng(b) = nwords. Returns the borrow bit
unsigned int mp_sub(const digit_t* a, const digit_t* b, digit_t* c, const unsigned int nwords);

// 736-bit multiprecision subtraction, c = a-b+2p or c = a-b+4p
extern void mp_sub736_p2(const digit_t* a, const digit_t* b, digit_t* c);
extern void mp_sub736_p4(const digit_t* a, const digit_t* b, digit_t* c);
void mp_sub736_p2_asm(const digit_t* a, const digit_t* b, digit_t* c);
void mp_sub736_p4_asm(const digit_t* a, const digit_t* b, digit_t* c);

// 2x736-bit multiprecision subtraction followed by addition with p736*2^768, c = a-b+(p736*2^768) if a-b < 0, otherwise c=a-b
void mp_subaddx2_asm(const digit_t* a, const digit_t* b, digit_t* c);
void mp_subadd736x2_asm(const digit_t* a, const digit_t* b, digit_t* c);

// Double 2x736-bit multiprecision subtraction, c = c-a-b, where c > a and c > b
void mp_dblsub736x2_asm(const digit_t* a, const digit_t* b, digit_t* c);

// Multiprecision left shift
void mp_shiftleft(digit_t* x, unsigned int shift, const unsigned int nwords);

// Multiprecision right shift by one
void mp_shiftr1(digit_t* x, const unsigned int nwords);

// Multiprecision left right shift by one
void mp_shiftl1(digit_t* x, const unsigned int nwords);

// Digit multiplication, digit * digit -> 2-digit result
void digit_x_digit(const digit_t a, const digit_t b, digit_t* c);

// Multiprecision comba multiply, c = a*b, where lng(a) = lng(b) = nwords.
void mp_mul(const digit_t* a, const digit_t* b, digit_t* c, const unsigned int nwords);

/************ Field arithmetic functions *************/

// Copy of a field element, c = a
void fpcopy736(const digit_t* a, digit_t* c);

// Zeroing a field element, a = 0
void fpzero736(digit_t* a);

// Non constant-time comparison of two field elements. If a = b return TRUE, otherwise, return FALSE
bool fpequal736_non_constant_time(const digit_t* a, const digit_t* b);

#ifdef PMNS_IMPLEMENTATION
void from_PMNS_736 (const felm_t pa, felm_t a);
void to_PMNS_736 (const felm_t a, felm_t pa);
void pmns_coeff_reduc (felm_t a);
void pmns_coeff_reduc_0 (felm_t a);
void pmns_coeff_reduc_012 (felm_t a);
#include <stdio.h>
void PMNS_fprintf (FILE *out, const digit_t *p, const unsigned int nwords);

void mp_sub736x2_pmns (const digit_t* a, const digit_t* b, digit_t* c);
void mp_dblsub736x2_pmns (const digit_t* a, const digit_t* b, digit_t* c);
void mp_sub736x2_pmns (const digit_t* a, const digit_t* b, digit_t* c);
#endif

// Modular addition, c = a+b mod p736
extern void fpadd736(const digit_t* a, const digit_t* b, digit_t* c);
extern void fpadd736_asm(const digit_t* a, const digit_t* b, digit_t* c);

// Modular subtraction, c = a-b mod p736
extern void fpsub736(const digit_t* a, const digit_t* b, digit_t* c);
extern void fpsub736_asm(const digit_t* a, const digit_t* b, digit_t* c);

// Modular negation, a = -a mod p736
extern void fpneg736(digit_t* a);

// Modular division by two, c = a/2 mod p736.
void fpdiv2_736(const digit_t* a, digit_t* c);

// Modular correction to reduce field element a in [0, 2*p736-1] to [0, p736-1].
void fpcorrection736(digit_t* a);

// 736-bit Montgomery reduction, c = a mod p
void rdc_mont(digit_t* a, digit_t* c);
void rdc736_asm(digit_t* ma, digit_t* mc);

// Field multiplication using Montgomery arithmetic, c = a*b*R^-1 mod p736, where R=2^768
void fpmul736_mont(const digit_t* a, const digit_t* b, digit_t* c);
void mul736_asm(const digit_t* a, const digit_t* b, digit_t* c);

// Field squaring using Montgomery arithmetic, c = a*b*R^-1 mod p736, where R=2^768
void fpsqr736_mont(const digit_t* ma, digit_t* mc);

// Conversion to Montgomery representation
void to_mont(const digit_t* a, digit_t* mc);

// Conversion from Montgomery representation to standard representation
void from_mont(const digit_t* ma, digit_t* c);

// Field inversion, a = a^-1 in GF(p736)
void fpinv736_mont(digit_t* a);

// Field inversion, a = a^-1 in GF(p736) using the binary GCD
void fpinv736_mont_bingcd(digit_t* a);

// Chain to compute (p736-3)/4 using Montgomery arithmetic
void fpinv736_chain_mont(digit_t* a);

/************ GF(p^2) arithmetic functions *************/

// Copy of a GF(p736^2) element, c = a
void fp2copy736(const f2elm_t a, f2elm_t c);

// Zeroing a GF(p736^2) element, a = 0
void fp2zero736(f2elm_t a);

// GF(p736^2) negation, a = -a in GF(p736^2)
void fp2neg736(f2elm_t a);

// GF(p736^2) addition, c = a+b in GF(p736^2)
extern void fp2add736(const f2elm_t a, const f2elm_t b, f2elm_t c);

// GF(p736^2) subtraction, c = a-b in GF(p736^2)
extern void fp2sub736(const f2elm_t a, const f2elm_t b, f2elm_t c);

// GF(p736^2) division by two, c = a/2  in GF(p736^2)
void fp2div2_736(const f2elm_t a, f2elm_t c);

// Modular correction, a = a in GF(p736^2)
void fp2correction736(f2elm_t a);

// GF(p736^2) squaring using Montgomery arithmetic, c = a^2 in GF(p736^2)
void fp2sqr736_mont(const f2elm_t a, f2elm_t c);

// GF(p736^2) multiplication using Montgomery arithmetic, c = a*b in GF(p736^2)
void fp2mul736_mont(const f2elm_t a, const f2elm_t b, f2elm_t c);

// Conversion of a GF(p736^2) element to Montgomery representation
void to_fp2mont(const f2elm_t a, f2elm_t mc);

// Conversion of a GF(p736^2) element from Montgomery representation to standard representation
void from_fp2mont(const f2elm_t ma, f2elm_t c);

// GF(p736^2) inversion using Montgomery arithmetic, a = (a0-i*a1)/(a0^2+a1^2)
void fp2inv736_mont(f2elm_t a);

// GF(p736^2) inversion, a = (a0-i*a1)/(a0^2+a1^2), GF(p736) inversion done using the binary GCD
void fp2inv736_mont_bingcd(f2elm_t a);

// n-way Montgomery inversion
void mont_n_way_inv(const f2elm_t* vec, const int n, f2elm_t* out);

/************ Elliptic curve and isogeny functions *************/

// Computes the j-invariant of a Montgomery curve with projective constant.
void j_inv(const f2elm_t A, const f2elm_t C, f2elm_t jinv);

// Simultaneous doubling and differential addition.
void xDBLADD(point_proj_t P, point_proj_t Q, const f2elm_t xPQ, const f2elm_t A24);

// Doubling of a Montgomery point in projective coordinates (X:Z).
void xDBL(const point_proj_t P, point_proj_t Q, const f2elm_t A24plus, const f2elm_t C24);

// Computes [2^e](X:Z) on Montgomery curve with projective constant via e repeated doublings.
void xDBLe(const point_proj_t P, point_proj_t Q, const f2elm_t A24plus, const f2elm_t C24, const int e);

// Differential addition.
void xADD(point_proj_t P, const point_proj_t Q, const f2elm_t xPQ);

// Computes the corresponding 4-isogeny of a projective Montgomery point (X4:Z4) of order 4.
void get_4_isog(const point_proj_t P, f2elm_t A24plus, f2elm_t C24, f2elm_t* coeff);

// Evaluates the isogeny at the point (X:Z) in the domain of the isogeny.
void eval_4_isog(point_proj_t P, f2elm_t* coeff);

// Tripling of a Montgomery point in projective coordinates (X:Z).
void xTPL(const point_proj_t P, point_proj_t Q, const f2elm_t A24minus, const f2elm_t A24plus);

// Computes [3^e](X:Z) on Montgomery curve with projective constant via e repeated triplings.
void xTPLe(const point_proj_t P, point_proj_t Q, const f2elm_t A24minus, const f2elm_t A24plus, const int e);

// Computes the corresponding 3-isogeny of a projective Montgomery point (X3:Z3) of order 3.
void get_3_isog(const point_proj_t P, f2elm_t A24minus, f2elm_t A24plus, f2elm_t* coeff);

// Computes the 3-isogeny R=phi(X:Z), given projective point (X3:Z3) of order 3 on a Montgomery curve and a point P with coefficients given in coeff.
void eval_3_isog(point_proj_t Q, const f2elm_t* coeff);

// 3-way simultaneous inversion
void inv_3_way(f2elm_t z1, f2elm_t z2, f2elm_t z3);

// Given the x-coordinates of P, Q, and R, returns the value A corresponding to the Montgomery curve E_A: y^2=x^3+A*x^2+x such that R=Q-P on E_A.
void get_A(const f2elm_t xP, const f2elm_t xQ, const f2elm_t xR, f2elm_t A);


#endif
