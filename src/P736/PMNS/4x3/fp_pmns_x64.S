/*
 * SIDH: an efficient supersingular isogeny cryptography library
 * x64 assembly code for P503 using PMNS
 */

.intel_syntax noprefix

/* Format function and variable names for Mac OS X */
#if defined(__APPLE__)
  #define fmt(f) _##f
#else
  #define fmt(f) f
#endif

/* Registers that are used for parameter passing: */
#define reg_p1 rdi
#define reg_p2 rsi
#define reg_p3 rdx

#include "pmns_internal.h"

.text

/*
 * Coefficient reduction in PMNS
 * Operation: a [reg_p2] = a [reg_p1]
 */
.global fmt(pmns_coeff_reduc)
fmt(pmns_coeff_reduc):
  mov   rax, 0x01ffffffffffffff
  lea   rdx, [rip+fmt(pmns_corrections2)]
  lea   rcx, [rip+fmt(pmns_carries)]

  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]

  mov   r11, r10
  and   r10, rax
  shr   r11, 57
  shl   r11, 1
  add   r9, [rdx+8*r11]
  adc   r10, [rdx+8*r11+0x8]

  mov   [reg_p1+0x8], r9
  mov   [reg_p1+0x10], r10

  mov   r8, [reg_p1+0x18]
  mov   r9, [reg_p1+0x20]
  mov   r10, [reg_p1+0x28]

  mov   rsi, r10
  and   r10, rax
  shr   rsi, 57
  shl   rsi, 1
  add   r8, [rcx+4*r11]
  adc   r9, [rdx+8*rsi]
  adc   r10, [rdx+8*rsi+0x8]

  mov   [reg_p1+0x18], r8
  mov   [reg_p1+0x20], r9
  mov   [reg_p1+0x28], r10

  mov   r8, [reg_p1+0x30]
  mov   r9, [reg_p1+0x38]
  mov   r10, [reg_p1+0x40]

  mov   r11, r10
  and   r10, rax
  shr   r11, 57
  shl   r11, 1
  add   r8, [rcx+4*rsi]
  adc   r9, [rdx+8*r11]
  adc   r10, [rdx+8*r11+0x8]

  mov   [reg_p1+0x30], r8
  mov   [reg_p1+0x38], r9
  mov   [reg_p1+0x40], r10

  mov   r8, [reg_p1+0x48]
  mov   r9, [reg_p1+0x50]
  mov   r10, [reg_p1+0x58]

  mov   rsi, r10
  and   r10, rax
  shr   rsi, 57
  shl   rsi, 1
  add   r8, [rcx+4*r11]
  adc   r9, [rdx+8*rsi]
  adc   r10, [rdx+8*rsi+0x8]

  mov   [reg_p1+0x48], r8
  mov   [reg_p1+0x50], r9
  mov   [reg_p1+0x58], r10

  mov   r8, [reg_p1]
  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]
  mov   r11, [rcx+4*rsi]
  shl   r11, 3
  add   r8, r11
  adc   r9, 0
  adc   r10, 0

  mov   [reg_p1], r8
  mov   [reg_p1+0x8], r9
  mov   [reg_p1+0x10], r10

  ret

/*
 * Coefficient reduction of coeff of degree 0, 1 and 2 in PMNS
 * Operation: a [reg_p2] = a [reg_p1]
 */
.global fmt(pmns_coeff_reduc_012)
fmt(pmns_coeff_reduc_012):
  mov   rax, 0x01ffffffffffffff
  lea   rdx, [rip+fmt(pmns_corrections2)]
  lea   rcx, [rip+fmt(pmns_carries)]

  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]

  mov   r11, r10
  and   r10, rax
  shr   r11, 57
  shl   r11, 1
  add   r9, [rdx+8*r11]
  adc   r10, [rdx+8*r11+0x8]

  mov   [reg_p1+0x8], r9
  mov   [reg_p1+0x10], r10

  mov   r8, [reg_p1+0x18]
  mov   r9, [reg_p1+0x20]
  mov   r10, [reg_p1+0x28]

  mov   rsi, r10
  and   r10, rax
  shr   rsi, 57
  shl   rsi, 1
  add   r8, [rcx+4*r11]
  adc   r9, [rdx+8*rsi]
  adc   r10, [rdx+8*rsi+0x8]

  mov   [reg_p1+0x18], r8
  mov   [reg_p1+0x20], r9
  mov   [reg_p1+0x28], r10

  mov   r8, [reg_p1+0x30]
  mov   r9, [reg_p1+0x38]
  mov   r10, [reg_p1+0x40]

  mov   r11, r10
  and   r10, rax
  shr   r11, 57
  shl   r11, 1
  add   r8, [rcx+4*rsi]
  adc   r9, [rdx+8*r11]
  adc   r10, [rdx+8*r11+0x8]

  mov   [reg_p1+0x30], r8
  mov   [reg_p1+0x38], r9
  mov   [reg_p1+0x40], r10

  mov   r8, [reg_p1+0x48]
  mov   r9, [reg_p1+0x50]
  mov   r10, [reg_p1+0x58]

  add   r8, [rcx+4*r11]
  adc   r9, 0
  adc   r10, 0

  mov   [reg_p1+0x48], r8
  mov   [reg_p1+0x50], r9
  mov   [reg_p1+0x58], r10

  ret

/*
 * Coefficient reduction of constant term in PMNS
 * Operation: a [reg_p2] = a [reg_p1]
 */
.global fmt(pmns_coeff_reduc_0)
fmt(pmns_coeff_reduc_0):
  mov   rax, 0x01ffffffffffffff
  lea   rdx, [rip+fmt(pmns_corrections2)]
  lea   rcx, [rip+fmt(pmns_carries)]

  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]

  mov   r11, r10
  and   r10, rax
  shr   r11, 57
  shl   r11, 1
  add   r9, [rdx+8*r11]
  adc   r10, [rdx+8*r11+0x8]

  mov   [reg_p1+0x8], r9
  mov   [reg_p1+0x10], r10

  mov   r8, [reg_p1+0x18]
  mov   r9, [reg_p1+0x20]
  mov   r10, [reg_p1+0x28]

  add   r8, [rcx+4*r11]
  adc   r9, 0
  adc   r10, 0

  mov   [reg_p1+0x18], r8
  mov   [reg_p1+0x20], r9
  mov   [reg_p1+0x28], r10

  ret


/*
 * Addition in GF(p) in PMNS
 * Operation: c [reg_p2] = a [reg_p1] + b [reg_p2]
 */
.global fmt(fpadd736)
fmt(fpadd736):
  push  rbx
  push  r12

  mov   rax, 0x01ffffffffffffff
  lea   rbx, [rip+fmt(pmns_corrections2)]
  lea   rcx, [rip+fmt(pmns_carries)]

  mov   r8, [reg_p1]
  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]

  add   r8, [reg_p2]
  adc   r9, [reg_p2+0x8]
  adc   r10, [reg_p2+0x10]
  mov   r11, r10
  and   r10, rax
  shr   r11, 57
  shl   r11, 1
  add   r9, [rbx+8*r11]
  adc   r10, [rbx+8*r11+0x8]

  mov   [reg_p3], r8
  mov   [reg_p3+0x8], r9
  mov   [reg_p3+0x10], r10

  mov   r8, [reg_p1+0x18]
  mov   r9, [reg_p1+0x20]
  mov   r10, [reg_p1+0x28]

  add   r8, [reg_p2+0x18]
  adc   r9, [reg_p2+0x20]
  adc   r10, [reg_p2+0x28]
  mov   r12, r10
  and   r10, rax
  shr   r12, 57
  shl   r12, 1
  add   r8, [rcx+4*r11]
  adc   r9, [rbx+8*r12]
  adc   r10, [rbx+8*r12+0x8]

  mov   [reg_p3+0x18], r8
  mov   [reg_p3+0x20], r9
  mov   [reg_p3+0x28], r10

  mov   r8, [reg_p1+0x30]
  mov   r9, [reg_p1+0x38]
  mov   r10, [reg_p1+0x40]

  add   r8, [reg_p2+0x30]
  adc   r9, [reg_p2+0x38]
  adc   r10, [reg_p2+0x40]
  mov   r11, r10
  and   r10, rax
  shr   r11, 57
  shl   r11, 1
  add   r8, [rcx+4*r12]
  adc   r9, [rbx+8*r11]
  adc   r10, [rbx+8*r11+0x8]

  mov   [reg_p3+0x30], r8
  mov   [reg_p3+0x38], r9
  mov   [reg_p3+0x40], r10

  mov   r8, [reg_p1+0x48]
  mov   r9, [reg_p1+0x50]
  mov   r10, [reg_p1+0x58]

  add   r8, [reg_p2+0x48]
  adc   r9, [reg_p2+0x50]
  adc   r10, [reg_p2+0x58]
  mov   r12, r10
  and   r10, rax
  shr   r12, 57
  shl   r12, 1
  add   r8, [rcx+4*r11]
  adc   r9, [rbx+8*r12]
  adc   r10, [rbx+8*r12+0x8]

  mov   [reg_p3+0x48], r8
  mov   [reg_p3+0x50], r9
  mov   [reg_p3+0x58], r10

  mov   r8, [reg_p3]
  mov   r9, [reg_p3+0x8]
  mov   r10, [reg_p3+0x10]
  mov   r11, [rcx+4*r12]
  shl   r11, 3
  add   r8, r11
  adc   r9, 0
  adc   r10, 0

  mov   [reg_p3], r8
  mov   [reg_p3+0x8], r9
  mov   [reg_p3+0x10], r10

  pop   r12
  pop   rbx

  ret

/*
 * Addition in GF(p) in PMNS
 * Operation: c [reg_p2] = a [reg_p1] + b [reg_p2]
 */
.global fmt(fpsub736)
fmt(fpsub736):
  push  rbx
  push  r12
  push  r13
  push  r14
  push  r15

  mov   rax, 0x01ffffffffffffff
  lea   rbx, [rip+fmt(pmns_corrections2)]
  lea   rcx, [rip+fmt(pmns_carries)]

  mov r13, 0xffffffffffffffe8
  mov r14, 0xf4970fad87ffffff
  mov r15, 0x0447c92776776d3f

  mov   r8, [reg_p1]
  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]

  add   r8, r13
  adc   r9, r14
  adc   r10, r15
  sub   r8, [reg_p2]
  sbb   r9, [reg_p2+0x8]
  sbb   r10, [reg_p2+0x10]
  mov   r11, r10
  and   r10, rax
  shr   r11, 57
  shl   r11, 1
  add   r9, [rbx+8*r11]
  adc   r10, [rbx+8*r11+0x8]

  mov   [reg_p3], r8
  mov   [reg_p3+0x8], r9
  mov   [reg_p3+0x10], r10

  mov r13, 0xfffffffffffffffd

  mov   r8, [reg_p1+0x18]
  mov   r9, [reg_p1+0x20]
  mov   r10, [reg_p1+0x28]

  add   r8, r13
  adc   r9, r14
  adc   r10, r15
  sub   r8, [reg_p2+0x18]
  sbb   r9, [reg_p2+0x20]
  sbb   r10, [reg_p2+0x28]
  mov   r12, r10
  and   r10, rax
  shr   r12, 57
  shl   r12, 1
  add   r8, [rcx+4*r11]
  add   r9, [rbx+8*r12]
  adc   r10, [rbx+8*r12+0x8]

  mov   [reg_p3+0x18], r8
  mov   [reg_p3+0x20], r9
  mov   [reg_p3+0x28], r10

  mov   r8, [reg_p1+0x30]
  mov   r9, [reg_p1+0x38]
  mov   r10, [reg_p1+0x40]

  add   r8, r13
  adc   r9, r14
  adc   r10, r15
  sub   r8, [reg_p2+0x30]
  sbb   r9, [reg_p2+0x38]
  sbb   r10, [reg_p2+0x40]
  mov   r11, r10
  and   r10, rax
  shr   r11, 57
  shl   r11, 1
  add   r8, [rcx+4*r12]
  add   r9, [rbx+8*r11]
  adc   r10, [rbx+8*r11+0x8]

  mov   [reg_p3+0x30], r8
  mov   [reg_p3+0x38], r9
  mov   [reg_p3+0x40], r10

  mov   r8, [reg_p1+0x48]
  mov   r9, [reg_p1+0x50]
  mov   r10, [reg_p1+0x58]

  add   r8, r13
  adc   r9, r14
  adc   r10, r15
  sub   r8, [reg_p2+0x48]
  sbb   r9, [reg_p2+0x50]
  sbb   r10, [reg_p2+0x58]
  mov   r12, r10
  and   r10, rax
  shr   r12, 57
  shl   r12, 1
  add   r8, [rcx+4*r11]
  add   r9, [rbx+8*r12]
  adc   r10, [rbx+8*r12+0x8]

  mov   [reg_p3+0x48], r8
  mov   [reg_p3+0x50], r9
  mov   [reg_p3+0x58], r10

  mov   r8, [reg_p3]
  mov   r9, [reg_p3+0x8]
  mov   r10, [reg_p3+0x10]
  mov   r11, [rcx+4*r12]
  shl   r11, 3
  add   r8, r11
  adc   r9, 0
  adc   r10, 0

  mov   [reg_p3], r8
  mov   [reg_p3+0x8], r9
  mov   [reg_p3+0x10], r10




  pop  r15
  pop  r14
  pop  r13
  pop  r12
  pop  rbx

  ret

/*
 * Division by 2 in GF(p) in PMNS
 * Operation: c [reg_p2] = a [reg_p1] / 2
 */
.global fmt(fpdiv2_736)
fmt(fpdiv2_736):
  push  r12
  push  r13

  mov   r8, 0xa6dd0539d8000000
  mov   r9, 0x016d430d277d246a

  mov   r12, [reg_p1+0x48]
  mov   r13, [reg_p1+0x50]
  mov   rdx, [reg_p1+0x58]
  shrd  r12, r13, 1
  sbb   rax, rax
  shrd  r13, rdx, 1
  shr   rdx, 1
  mov   [reg_p2+0x48], r12

  mov   r10, [reg_p1+0x30]
  mov   r11, [reg_p1+0x38]
  mov   r12, [reg_p1+0x40]
  mov   rcx, rax
  and   rax, r8
  and   rcx, r9
  add   r11, rax
  adc   r12, rcx
  shrd  r10, r11, 1
  sbb   rax, rax
  shrd  r11, r12, 1
  shr   r12, 1
  mov   [reg_p2+0x30], r10
  mov   [reg_p2+0x38], r11
  mov   [reg_p2+0x40], r12

  mov   r10, [reg_p1+0x18]
  mov   r11, [reg_p1+0x20]
  mov   r12, [reg_p1+0x28]
  mov   rcx, rax
  and   rax, r8
  and   rcx, r9
  add   r11, rax
  adc   r12, rcx
  shrd  r10, r11, 1
  sbb   rax, rax
  shrd  r11, r12, 1
  shr   r12, 1
  mov   [reg_p2+0x18], r10
  mov   [reg_p2+0x20], r11
  mov   [reg_p2+0x28], r12

  mov   r10, [reg_p1]
  mov   r11, [reg_p1+0x08]
  mov   r12, [reg_p1+0x10]
  mov   rcx, rax
  and   rax, r8
  and   rcx, r9
  add   r11, rax
  adc   r12, rcx
  shrd  r10, r11, 1
  sbb   rax, rax
  shrd  r11, r12, 1
  shr   r12, 1
  mov   [reg_p2], r10
  mov   [reg_p2+0x08], r11
  mov   [reg_p2+0x10], r12

  mov   r8, 0xaa6dd0539d800000
  mov   r9, 0x0016d430d277d246
  and   r8, rax
  and   r9, rax
  add   r13, r8
  adc   rdx, r9
  mov   [reg_p2+0x50], r13
  mov   [reg_p2+0x58], rdx

  pop   r13
  pop   r12
  ret

/*
 * Schoolbook multiplication for integers of 192 bits.
 * rdx is reserved for internal usage, registers parameters must not be rdx.
 * rax is reserved for internal usage, registers parameters must not be rax.
 * The result is written in R0, R1, R2, R3, R4 and rax.
 */

.macro MUL192_SCHOOL M0, M1, R0, R1, R2, R3, R4, T0, T1, T2, T3
  mov    rdx, \M0
  mulx   \R1, \R0, \M1      /* R1:R0 = A0*B0 */     /* R0 final */
  mulx   \R3, \T0, 8\M1     /* R3:T0 = A0*B1 */
  xor    rax, rax
  adox   \R1, \T0
  mulx   \T0, \R4, 16\M1    /* T0:R4 = A0*B2 */
  adox   \R3, \R4

  mov    rdx, 8\M0
  mulx   \R4, \T1, \M1      /* R4:T1 = A1*B0 */
  adox   \T0, rax
  xor    rax, rax
  mulx   \T2, \T3, 8\M1     /* T2:T3 = A1*B1 */
  adox   \R1, \T1                                   /* R1 final */
  adcx   \R4, \T3
  mulx   \T3, \R2, 16\M1   /* T3:R2 = A1*B2 */
  adox   \R4, \R3
  adcx   \T2, \R2
  adcx   \T3, rax
  adox   \T2, \T0

  mov    rdx, 16\M0
  mulx   \R3, \R2, \M1      /* R3:R2 = A2*B0 */
  adox   \T3, rax
  xor    rax, rax
  mulx   \T1, \T0, 8\M1     /* T1:T0 = A2*B1 */
  adox   \R2, \R4                                   /* R2 final */
  adcx   \R3, \T2
  mulx   \T2, \R4, 16\M1    /* T2:R4 = A2*B2 */
  adcx   \T1, \T3
  adcx   \T2, rax
  adox   \R3, \T0                                   /* R3 final */
  adox   \R4, \T1                                   /* R4 final */
  adox   rax, \T2                                   /* R5 (=rax) final */
.endm

/*
 * Input is in registers R0, R1, R2 and in immediate constants, I0, I1
 * Output is written in T0, T1, T2, T3, T4
 * All registers must be different.
 * Input registers may be overwritten.
 * rdx is reserved for internal usage, registers parameters must not be rdx.
 */
.macro MUL192x128imm_SCHOOL R0, R1, R2, T0, T1, T2, T3, T4, Tt, I0, I1
  mov    rdx, \I0
  mulx   \T1, \T0, \R0     /* T1:T0 = I0*R0 */
  mulx   \T2, \Tt, \R1     /* T2:Tt = I0*R1 */
  xor    rax, rax
  adox   \T1, \Tt
  mulx   \T3, \Tt, \R2     /* T3:Tt = I0*R2 */
  adox   \T2, \Tt

  mov    rdx, \I1
  mulx   \R0, \T4, \R0     /* R0:T4 = I1*R0 */
  adox   \T3, rax
  xor    rax, rax
  mulx   \R1, \Tt, \R1     /* R1:Tt = I1*R1 */
  adox   \T1, \T4
  adcx   \R0, \Tt
  mulx   \T4, \R2, \R2     /* T4:R2 = I1*R2 */
  adox   \T2, \R0
  adcx   \R1, \R2
  adcx   \T4, rax
  adox   \T3, \R1
  adox   \T4, rax
.endm

/*
 * Montgomery reduction in PMNS
 * Operation: c [reg_p2] = redc(a [reg_p1])
 */
.global fmt(rdc_mont)
fmt(rdc_mont):
  push  r12
  push  r13
  push  r14
  push  r15
  push  rbx

  mov   r8, [reg_p1+0x90]
  mov   r9, [reg_p1+0x98]
  mov   r10, [reg_p1+0xa0]

  mov   r11, [reg_p1]
  mov   r12, [reg_p1+0x8]

  mov   rdx, 0x54dba0a73b000000
  mulx  r14, r13, r11
  add   r9, r13
  adc   r10, r14

  mulx  r13, r12, r12
  add   r10, r12

  mov   rdx, 0x002da861a4efa48d
  mulx  r13, r12, r11
  add   r10, r12

  mov   r11, [reg_p1+0x30]
  mov   rdx, 0x3cc8000000000000
  mulx  r13, r12, r11
  add   r10, r12

  MUL192x128imm_SCHOOL r8, r9, r10, r11, r12, r13, r14, r15, rbx, 0xa6dd0539d8000000, 0x016d430d277d246a

  mov   r10, [reg_p1+0x60]
  add   r11, [reg_p1+0x68]
  adc   r12, [reg_p1+0x70]
  adc   r13, [reg_p1+0x78]
  adc   r14, [reg_p1+0x80]
  adc   r15, [reg_p1+0x88]

  mov   [reg_p2+0x30], r13
  mov   [reg_p2+0x38], r14
  mov   [reg_p2+0x40], r15

  MUL192x128imm_SCHOOL r10, r11, r12, r8, r9, r13, r14, r15, rbx, 0xa6dd0539d8000000, 0x016d430d277d246a

  mov   rbx, [reg_p1+0x30]
  add   r8, [reg_p1+0x38]
  adc   r9, [reg_p1+0x40]
  adc   r13, [reg_p1+0x48]
  adc   r14, [reg_p1+0x50]
  adc   r15, [reg_p1+0x58]

  mov   [reg_p2+0x18], r13
  mov   [reg_p2+0x20], r14
  mov   [reg_p2+0x28], r15

  MUL192x128imm_SCHOOL rbx, r8, r9, r10, r11, r12, r13, r14, r15, 0xa6dd0539d8000000, 0x016d430d277d246a

  mov   rbx, [reg_p1]
  add   r10, [reg_p1+0x8]
  adc   r11, [reg_p1+0x10]
  adc   r12, [reg_p1+0x18]
  adc   r13, [reg_p1+0x20]
  adc   r14, [reg_p1+0x28]

  mov   [reg_p2], r12
  mov   [reg_p2+0x8], r13
  mov   [reg_p2+0x10], r14

  MUL192x128imm_SCHOOL rbx, r10, r11, r8, r9, r12, r13, r14, r15, 0x54dba0a73b000000, 0x002da861a4efa48d

  add   r8, [reg_p1+0x98]
  adc   r9, [reg_p1+0xa0]
  adc   r12, [reg_p1+0xa8]
  adc   r13, [reg_p1+0xb0]
  adc   r14, [reg_p1+0xb8]

  mov   [reg_p2+0x48], r12
  mov   [reg_p2+0x50], r13
  mov   [reg_p2+0x58], r14

  pop  rbx
  pop  r15
  pop  r14
  pop  r13
  pop  r12

  ret

#define USE_OPTIMAL_POLYMUL
#define KARATSUBA

#ifndef USE_OPTIMAL_POLYMUL
/*
 * Multiplication in GF(p) in PMNS (using schoolbook algorithm)
 * Operation: c [reg_p3] = a [reg_p1] * b [reg_p2]
 */
.global fmt(mp_mul)
fmt(mp_mul):
  push  r12
  push  r13
  push  r14
  push  r15
  push  rbx

  mov   rcx, reg_p3

  MUL192_SCHOOL [reg_p1], [reg_p2], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   [rcx], r8
  mov   [rcx+0x8], r9
  mov   [rcx+0x10], r10
  mov   [rcx+0x18], r11
  mov   [rcx+0x20], r12
  mov   [rcx+0x28], rax

  MUL192_SCHOOL [reg_p1+0x18], [reg_p2], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   [rcx+0x30], r8
  mov   [rcx+0x38], r9
  mov   [rcx+0x40], r10
  mov   [rcx+0x48], r11
  mov   [rcx+0x50], r12
  mov   [rcx+0x58], rax

  MUL192_SCHOOL [reg_p1+0x30], [reg_p2], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   [rcx+0x60], r8
  mov   [rcx+0x68], r9
  mov   [rcx+0x70], r10
  mov   [rcx+0x78], r11
  mov   [rcx+0x80], r12
  mov   [rcx+0x88], rax

  MUL192_SCHOOL [reg_p1+0x48], [reg_p2], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   [rcx+0x90], r8
  mov   [rcx+0x98], r9
  mov   [rcx+0xa0], r10
  mov   [rcx+0xa8], r11
  mov   [rcx+0xb0], r12
  mov   [rcx+0xb8], rax

  MUL192_SCHOOL [reg_p1], [reg_p2+0x18], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  add   [rcx+0x30], r8
  adc   [rcx+0x38], r9
  adc   [rcx+0x40], r10
  adc   [rcx+0x48], r11
  adc   [rcx+0x50], r12
  adc   [rcx+0x58], rax

  MUL192_SCHOOL [reg_p1+0x18], [reg_p2+0x18], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  add   [rcx+0x60], r8
  adc   [rcx+0x68], r9
  adc   [rcx+0x70], r10
  adc   [rcx+0x78], r11
  adc   [rcx+0x80], r12
  adc   [rcx+0x88], rax

  MUL192_SCHOOL [reg_p1+0x30], [reg_p2+0x18], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  add   [rcx+0x90], r8
  adc   [rcx+0x98], r9
  adc   [rcx+0xa0], r10
  adc   [rcx+0xa8], r11
  adc   [rcx+0xb0], r12
  adc   [rcx+0xb8], rax

  MUL192_SCHOOL [reg_p1+0x48], [reg_p2+0x18], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  shld  rax, r12, 3
  shld  r12, r11, 3
  shld  r11, r10, 3
  shld  r10, r9, 3
  shld  r9, r8, 3
  shl   r8, 3

  add   [rcx], r8
  adc   [rcx+0x8], r9
  adc   [rcx+0x10], r10
  adc   [rcx+0x18], r11
  adc   [rcx+0x20], r12
  adc   [rcx+0x28], rax

  MUL192_SCHOOL [reg_p1], [reg_p2+0x30], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  add   [rcx+0x60], r8
  adc   [rcx+0x68], r9
  adc   [rcx+0x70], r10
  adc   [rcx+0x78], r11
  adc   [rcx+0x80], r12
  adc   [rcx+0x88], rax

  MUL192_SCHOOL [reg_p1+0x18], [reg_p2+0x30], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  add   [rcx+0x90], r8
  adc   [rcx+0x98], r9
  adc   [rcx+0xa0], r10
  adc   [rcx+0xa8], r11
  adc   [rcx+0xb0], r12
  adc   [rcx+0xb8], rax

  MUL192_SCHOOL [reg_p1+0x30], [reg_p2+0x30], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  shld  rax, r12, 3
  shld  r12, r11, 3
  shld  r11, r10, 3
  shld  r10, r9, 3
  shld  r9, r8, 3
  shl   r8, 3

  add   [rcx], r8
  adc   [rcx+0x8], r9
  adc   [rcx+0x10], r10
  adc   [rcx+0x18], r11
  adc   [rcx+0x20], r12
  adc   [rcx+0x28], rax

  MUL192_SCHOOL [reg_p1+0x48], [reg_p2+0x30], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  shld  rax, r12, 3
  shld  r12, r11, 3
  shld  r11, r10, 3
  shld  r10, r9, 3
  shld  r9, r8, 3
  shl   r8, 3

  add   [rcx+0x30], r8
  adc   [rcx+0x38], r9
  adc   [rcx+0x40], r10
  adc   [rcx+0x48], r11
  adc   [rcx+0x50], r12
  adc   [rcx+0x58], rax

  MUL192_SCHOOL [reg_p1], [reg_p2+0x48], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  add   [rcx+0x90], r8
  adc   [rcx+0x98], r9
  adc   [rcx+0xa0], r10
  adc   [rcx+0xa8], r11
  adc   [rcx+0xb0], r12
  adc   [rcx+0xb8], rax

  MUL192_SCHOOL [reg_p1+0x18], [reg_p2+0x48], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  shld  rax, r12, 3
  shld  r12, r11, 3
  shld  r11, r10, 3
  shld  r10, r9, 3
  shld  r9, r8, 3
  shl   r8, 3

  add   [rcx], r8
  adc   [rcx+0x8], r9
  adc   [rcx+0x10], r10
  adc   [rcx+0x18], r11
  adc   [rcx+0x20], r12
  adc   [rcx+0x28], rax

  MUL192_SCHOOL [reg_p1+0x30], [reg_p2+0x48], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  shld  rax, r12, 3
  shld  r12, r11, 3
  shld  r11, r10, 3
  shld  r10, r9, 3
  shld  r9, r8, 3
  shl   r8, 3

  add   [rcx+0x30], r8
  adc   [rcx+0x38], r9
  adc   [rcx+0x40], r10
  adc   [rcx+0x48], r11
  adc   [rcx+0x50], r12
  adc   [rcx+0x58], rax

  MUL192_SCHOOL [reg_p1+0x48], [reg_p2+0x48], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  shld  rax, r12, 3
  shld  r12, r11, 3
  shld  r11, r10, 3
  shld  r10, r9, 3
  shld  r9, r8, 3
  shl   r8, 3

  add   [rcx+0x60], r8
  adc   [rcx+0x68], r9
  adc   [rcx+0x70], r10
  adc   [rcx+0x78], r11
  adc   [rcx+0x80], r12
  adc   [rcx+0x88], rax

  pop  rbx
  pop  r15
  pop  r14
  pop  r13
  pop  r12

  ret
#elif defined(KARATSUBA)

/*
 * Schoolbook multiplication for polynomial of degree 2
 * rdx is reserved for internal usage, registers parameters must not be rdx.
 * rax is reserved for internal usage, registers parameters must not be rax.
 */
.macro MUL_DEG2_SCHOOL A0, A1, B0, B1, R, T0, T1, T2, T3, T4, T5, T6, T7, T8
    MUL192_SCHOOL \A0, \B0, \T0, \T1, \T2, \T3, \T4, \T5, \T6, \T7, \T8

    mov   \R, \T0
    mov   0x08\R, \T1
    mov   0x10\R, \T2
    mov   0x18\R, \T3
    mov   0x20\R, \T4
    mov   0x28\R, rax

    MUL192_SCHOOL \A0, \B1, \T0, \T1, \T2, \T3, \T4, \T5, \T6, \T7, \T8

    mov   0x30\R, \T0
    mov   0x38\R, \T1
    mov   0x40\R, \T2
    mov   0x48\R, \T3
    mov   0x50\R, \T4
    mov   0x58\R, rax

    MUL192_SCHOOL \A1, \B0, \T0, \T1, \T2, \T3, \T4, \T5, \T6, \T7, \T8

    add   \T0, 0x30\R
    adc   \T1, 0x38\R
    adc   \T2, 0x40\R
    adc   \T3, 0x48\R
    adc   \T4, 0x50\R
    adc   rax, 0x58\R

    mov   0x30\R, \T0
    mov   0x38\R, \T1
    mov   0x40\R, \T2
    mov   0x48\R, \T3
    mov   0x50\R, \T4
    mov   0x58\R, rax

    MUL192_SCHOOL \A1, \B1, \T0, \T1, \T2, \T3, \T4, \T5, \T6, \T7, \T8

    mov   0x60\R, \T0
    mov   0x68\R, \T1
    mov   0x70\R, \T2
    mov   0x78\R, \T3
    mov   0x80\R, \T4
    mov   0x88\R, rax
.endm

#define BASECASE_MACRO MUL_DEG2_SCHOOL

/*
 * Multiplication in GF(p) in PMNS (with one level of Karatsuba)
 * Operation: c [reg_p3] = a [reg_p1] * b [reg_p2]
 */
.global fmt(mp_mul)
fmt(mp_mul):
  push  r12
  push  r13
  push  r14
  push  r15
  push  rbx

  mov   rcx, reg_p3
  sub   rsp, 0x120

  mov   r8, [reg_p1]
  mov   r9, [reg_p1+0x08]
  mov   r10, [reg_p1+0x10]
  mov   r11, [reg_p1+0x18]
  mov   r12, [reg_p1+0x20]
  mov   r13, [reg_p1+0x28]
  add   r8, [reg_p1+0x30]
  adc   r9, [reg_p1+0x38]
  adc   r10, [reg_p1+0x40]
  add   r11, [reg_p1+0x48]
  adc   r12, [reg_p1+0x50]
  adc   r13, [reg_p1+0x58]
  mov   [rsp], r8
  mov   [rsp+0x08], r9
  mov   [rsp+0x10], r10
  mov   [rsp+0x18], r11
  mov   [rsp+0x20], r12
  mov   [rsp+0x28], r13

  mov   r8, [reg_p2]
  mov   r9, [reg_p2+0x08]
  mov   r10, [reg_p2+0x10]
  mov   r11, [reg_p2+0x18]
  mov   r12, [reg_p2+0x20]
  mov   r13, [reg_p2+0x28]
  add   r8, [reg_p2+0x30]
  adc   r9, [reg_p2+0x38]
  adc   r10, [reg_p2+0x40]
  add   r11, [reg_p2+0x48]
  adc   r12, [reg_p2+0x50]
  adc   r13, [reg_p2+0x58]
  mov   [rsp+0x30], r8
  mov   [rsp+0x38], r9
  mov   [rsp+0x40], r10
  mov   [rsp+0x48], r11
  mov   [rsp+0x50], r12
  mov   [rsp+0x58], r13

  BASECASE_MACRO [rsp], [rsp+0x18], [rsp+0x30], [rsp+0x48], [rsp+0x90], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  BASECASE_MACRO [reg_p1], [reg_p1+0x18], [reg_p2], [reg_p2+0x18], [rcx], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  BASECASE_MACRO [reg_p1+0x30], [reg_p1+0x48], [reg_p2+0x30], [reg_p2+0x48], [rsp], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   r14, [rsp+0xc0]
  mov   r15, [rsp+0xc8]
  mov   rax, [rsp+0xd0]
  mov   rbx, [rsp+0xd8]
  mov   rdi, [rsp+0xe0]
  mov   rsi, [rsp+0xe8]

  sub   r14, [rsp+0x30]
  sbb   r15, [rsp+0x38]
  sbb   rax, [rsp+0x40]
  sbb   rbx, [rsp+0x48]
  sbb   rdi, [rsp+0x50]
  sbb   rsi, [rsp+0x58]

  mov   r8, [rsp+0x90]
  mov   r9, [rsp+0x98]
  mov   r10, [rsp+0xa0]
  mov   r11, [rsp+0xa8]
  mov   r12, [rsp+0xb0]
  mov   r13, [rsp+0xb8]

  sub   r14, [rcx+0x30]
  sbb   r15, [rcx+0x38]
  sbb   rax, [rcx+0x40]
  sbb   rbx, [rcx+0x48]
  sbb   rdi, [rcx+0x50]
  sbb   rsi, [rcx+0x58]

  mov   [rcx+0x90], r14
  mov   [rcx+0x98], r15
  mov   [rcx+0xa0], rax
  mov   [rcx+0xa8], rbx
  mov   [rcx+0xb0], rdi
  mov   [rcx+0xb8], rsi

  sub   r8, [rcx]
  sbb   r9, [rcx+0x08]
  sbb   r10, [rcx+0x10]
  sbb   r11, [rcx+0x18]
  sbb   r12, [rcx+0x20]
  sbb   r13, [rcx+0x28]

  mov   r14, [rsp+0xf0]
  mov   r15, [rsp+0xf8]
  mov   rax, [rsp+0x100]
  mov   rbx, [rsp+0x108]
  mov   rdi, [rsp+0x110]
  mov   rsi, [rsp+0x118]

  sub   r8, [rsp]
  sbb   r9, [rsp+0x08]
  sbb   r10, [rsp+0x10]
  sbb   r11, [rsp+0x18]
  sbb   r12, [rsp+0x20]
  sbb   r13, [rsp+0x28]

  sub   r14, [rsp+0x60]
  sbb   r15, [rsp+0x68]
  sbb   rax, [rsp+0x70]
  sbb   rbx, [rsp+0x78]
  sbb   rdi, [rsp+0x80]
  sbb   rsi, [rsp+0x88]

  sub   r14, [rcx+0x60]
  sbb   r15, [rcx+0x68]
  sbb   rax, [rcx+0x70]
  sbb   rbx, [rcx+0x78]
  sbb   rdi, [rcx+0x80]
  sbb   rsi, [rcx+0x88]

  add   r14, [rsp]
  adc   r15, [rsp+0x08]
  adc   rax, [rsp+0x10]
  adc   rbx, [rsp+0x18]
  adc   rdi, [rsp+0x20]
  adc   rsi, [rsp+0x28]

  shld  rsi, rdi, 3
  shld  rdi, rbx, 3
  shld  rbx, rax, 3
  shld  rax, r15, 3
  shld  r15, r14, 3
  shl   r14, 3

  add   r14, [rcx]
  adc   r15, [rcx+0x08]
  adc   rax, [rcx+0x10]
  adc   rbx, [rcx+0x18]
  adc   rdi, [rcx+0x20]
  adc   rsi, [rcx+0x28]

  mov   [rcx], r14
  mov   [rcx+0x08], r15
  mov   [rcx+0x10], rax
  mov   [rcx+0x18], rbx
  mov   [rcx+0x20], rdi
  mov   [rcx+0x28], rsi

  mov   r14, [rsp+0x60]
  mov   r15, [rsp+0x68]
  mov   rax, [rsp+0x70]
  mov   rbx, [rsp+0x78]
  mov   rdi, [rsp+0x80]
  mov   rsi, [rsp+0x88]

  shld  rsi, rdi, 3
  shld  rdi, rbx, 3
  shld  rbx, rax, 3
  shld  rax, r15, 3
  shld  r15, r14, 3
  shl   r14, 3

  add   r14, r8
  adc   r15, r9
  adc   rax, r10
  adc   rbx, r11
  adc   rdi, r12
  adc   rsi, r13

  add   r14, [rcx+0x60]
  adc   r15, [rcx+0x68]
  adc   rax, [rcx+0x70]
  adc   rbx, [rcx+0x78]
  adc   rdi, [rcx+0x80]
  adc   rsi, [rcx+0x88]

  mov   [rcx+0x60], r14
  mov   [rcx+0x68], r15
  mov   [rcx+0x70], rax
  mov   [rcx+0x78], rbx
  mov   [rcx+0x80], rdi
  mov   [rcx+0x88], rsi

  mov   r14, [rsp+0x30]
  mov   r15, [rsp+0x38]
  mov   rax, [rsp+0x40]
  mov   rbx, [rsp+0x48]
  mov   rdi, [rsp+0x50]
  mov   rsi, [rsp+0x58]

  shld  rsi, rdi, 3
  shld  rdi, rbx, 3
  shld  rbx, rax, 3
  shld  rax, r15, 3
  shld  r15, r14, 3
  shl   r14, 3

  mov   r8, [rcx+0x30]
  mov   r9, [rcx+0x38]
  mov   r10, [rcx+0x40]
  mov   r11, [rcx+0x48]
  mov   r12, [rcx+0x50]
  mov   r13, [rcx+0x58]

  add   r14, r8
  adc   r15, r9
  adc   rax, r10
  adc   rbx, r11
  adc   rdi, r12
  adc   rsi, r13

  mov   [rcx+0x30], r14
  mov   [rcx+0x38], r15
  mov   [rcx+0x40], rax
  mov   [rcx+0x48], rbx
  mov   [rcx+0x50], rdi
  mov   [rcx+0x58], rsi


  add   rsp, 0x120

  pop   rbx
  pop   r15
  pop   r14
  pop   r13
  pop   r12

  ret

#else
/*
 * Multiplication in GF(p) in PMNS
 * Operation: c [reg_p3] = a [reg_p1] * b [reg_p2]
 */
.global fmt(mp_mul)
fmt(mp_mul):
  push  r12
  push  r13
  push  r14
  push  r15
  push  rbx

  mov   rcx, reg_p3

  sub   rsp, 0x60

  mov   r8, [reg_p1]
  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]
  mov   r11, [reg_p1+0x18]
  mov   r12, [reg_p1+0x20]
  mov   r13, [reg_p1+0x28]
  add   r8, [reg_p1+0x30]
  adc   r9, [reg_p1+0x38]
  adc   r10, [reg_p1+0x40]
  add   r11, [reg_p1+0x48]
  adc   r12, [reg_p1+0x50]
  adc   r13, [reg_p1+0x58]
  mov   [rsp], r8
  mov   [rsp+0x8], r9
  mov   [rsp+0x10], r10
  mov   [rsp+0x18], r11
  mov   [rsp+0x20], r12
  mov   [rsp+0x28], r13

  mov   r8, [reg_p2]
  mov   r9, [reg_p2+0x8]
  mov   r10, [reg_p2+0x10]
  mov   r11, [reg_p2+0x18]
  mov   r12, [reg_p2+0x20]
  mov   r13, [reg_p2+0x28]
  add   r8, [reg_p2+0x30]
  adc   r9, [reg_p2+0x38]
  adc   r10, [reg_p2+0x40]
  add   r11, [reg_p2+0x48]
  adc   r12, [reg_p2+0x50]
  adc   r13, [reg_p2+0x58]
  mov   [rsp+0x30], r8
  mov   [rsp+0x38], r9
  mov   [rsp+0x40], r10
  mov   [rsp+0x48], r11
  mov   [rsp+0x50], r12
  mov   [rsp+0x58], r13


  MUL192_SCHOOL [rsp], [rsp+0x30], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   [rcx+0x60], r8
  mov   [rcx+0x68], r9
  mov   [rcx+0x70], r10
  mov   [rcx+0x78], r11
  mov   [rcx+0x80], r12
  mov   [rcx+0x88], rax

  MUL192_SCHOOL [rsp+0x18], [rsp+0x30], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   [rcx+0x90], r8
  mov   [rcx+0x98], r9
  mov   [rcx+0xa0], r10
  mov   [rcx+0xa8], r11
  mov   [rcx+0xb0], r12
  mov   [rcx+0xb8], rax

  MUL192_SCHOOL [rsp], [rsp+0x48], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  add   [rcx+0x90], r8
  adc   [rcx+0x98], r9
  adc   [rcx+0xa0], r10
  adc   [rcx+0xa8], r11
  adc   [rcx+0xb0], r12
  adc   [rcx+0xb8], rax

  MUL192_SCHOOL [rsp+0x18], [rsp+0x48], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   [rsp], r8
  mov   [rsp+0x8], r9
  mov   [rsp+0x10], r10
  mov   [rsp+0x18], r11
  mov   [rsp+0x20], r12
  mov   [rsp+0x28], rax

  MUL192_SCHOOL [reg_p1], [reg_p2], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  sub   [rcx+0x60], r8
  sbb   [rcx+0x68], r9
  sbb   [rcx+0x70], r10
  sbb   [rcx+0x78], r11
  sbb   [rcx+0x80], r12
  sbb   [rcx+0x88], rax

  mov   [rcx], r8
  mov   [rcx+0x8], r9
  mov   [rcx+0x10], r10
  mov   [rcx+0x18], r11
  mov   [rcx+0x20], r12
  mov   [rcx+0x28], rax

  MUL192_SCHOOL [reg_p1+0x18], [reg_p2], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   [rcx+0x30], r8
  mov   [rcx+0x38], r9
  mov   [rcx+0x40], r10
  mov   [rcx+0x48], r11
  mov   [rcx+0x50], r12
  mov   [rcx+0x58], rax

  MUL192_SCHOOL [reg_p1], [reg_p2+0x18], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  add   [rcx+0x30], r8
  adc   [rcx+0x38], r9
  adc   [rcx+0x40], r10
  adc   [rcx+0x48], r11
  adc   [rcx+0x50], r12
  adc   [rcx+0x58], rax

  mov   r8, [rcx+0x30]
  mov   r9, [rcx+0x38]
  mov   r10, [rcx+0x40]
  mov   r11, [rcx+0x48]
  mov   r12, [rcx+0x50]
  mov   rax, [rcx+0x58]

  sub   [rcx+0x90], r8
  sbb   [rcx+0x98], r9
  sbb   [rcx+0xa0], r10
  sbb   [rcx+0xa8], r11
  sbb   [rcx+0xb0], r12
  sbb   [rcx+0xb8], rax

  MUL192_SCHOOL [reg_p1+0x18], [reg_p2+0x18], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  sub   [rsp], r8
  sbb   [rsp+0x8], r9
  sbb   [rsp+0x10], r10
  sbb   [rsp+0x18], r11
  sbb   [rsp+0x20], r12
  sbb   [rsp+0x28], rax

  add   [rcx+0x60], r8
  adc   [rcx+0x68], r9
  adc   [rcx+0x70], r10
  adc   [rcx+0x78], r11
  adc   [rcx+0x80], r12
  adc   [rcx+0x88], rax


  MUL192_SCHOOL [reg_p1+0x30], [reg_p2+0x30], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  sub   [rcx+0x60], r8
  sbb   [rcx+0x68], r9
  sbb   [rcx+0x70], r10
  sbb   [rcx+0x78], r11
  sbb   [rcx+0x80], r12
  sbb   [rcx+0x88], rax

  add   [rsp], r8
  adc   [rsp+0x08], r9
  adc   [rsp+0x10], r10
  adc   [rsp+0x18], r11
  adc   [rsp+0x20], r12
  adc   [rsp+0x28], rax

  MUL192_SCHOOL [reg_p1+0x48], [reg_p2+0x48], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  sub   [rsp], r8
  sbb   [rsp+0x8], r9
  sbb   [rsp+0x10], r10
  sbb   [rsp+0x18], r11
  sbb   [rsp+0x20], r12
  sbb   [rsp+0x28], rax

  shld  rax, r12, 3
  shld  r12, r11, 3
  shld  r11, r10, 3
  shld  r10, r9, 3
  shld  r9, r8, 3
  shl   r8, 3

  add   [rcx+0x60], r8
  adc   [rcx+0x68], r9
  adc   [rcx+0x70], r10
  adc   [rcx+0x78], r11
  adc   [rcx+0x80], r12
  adc   [rcx+0x88], rax

  mov   r8, [rsp]
  mov   r9, [rsp+0x8]
  mov   r10, [rsp+0x10]
  mov   r11, [rsp+0x18]
  mov   r12, [rsp+0x20]
  mov   rax, [rsp+0x28]

  shld  rax, r12, 3
  shld  r12, r11, 3
  shld  r11, r10, 3
  shld  r10, r9, 3
  shld  r9, r8, 3
  shl   r8, 3

  add   [rcx], r8
  adc   [rcx+0x8], r9
  adc   [rcx+0x10], r10
  adc   [rcx+0x18], r11
  adc   [rcx+0x20], r12
  adc   [rcx+0x28], rax


  MUL192_SCHOOL [reg_p1+0x48], [reg_p2+0x30], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  mov   [rsp], r8
  mov   [rsp+0x8], r9
  mov   [rsp+0x10], r10
  mov   [rsp+0x18], r11
  mov   [rsp+0x20], r12
  mov   [rsp+0x28], rax

  MUL192_SCHOOL [reg_p1+0x30], [reg_p2+0x48], r8, r9, r10, r11, r12, r13, r14, r15, rbx

  add   [rsp], r8
  adc   [rsp+0x8], r9
  adc   [rsp+0x10], r10
  adc   [rsp+0x18], r11
  adc   [rsp+0x20], r12
  adc   [rsp+0x28], rax

  mov   r8, [rsp]
  mov   r9, [rsp+0x8]
  mov   r10, [rsp+0x10]
  mov   r11, [rsp+0x18]
  mov   r12, [rsp+0x20]
  mov   rax, [rsp+0x28]

  sub   [rcx+0x90], r8
  sbb   [rcx+0x98], r9
  sbb   [rcx+0xa0], r10
  sbb   [rcx+0xa8], r11
  sbb   [rcx+0xb0], r12
  sbb   [rcx+0xb8], rax

  shld  rax, r12, 3
  shld  r12, r11, 3
  shld  r11, r10, 3
  shld  r10, r9, 3
  shld  r9, r8, 3
  shl   r8, 3

  add   [rcx+0x30], r8
  adc   [rcx+0x38], r9
  adc   [rcx+0x40], r10
  adc   [rcx+0x48], r11
  adc   [rcx+0x50], r12
  adc   [rcx+0x58], rax

  add   rsp, 0x60

  pop  rbx
  pop  r15
  pop  r14
  pop  r13
  pop  r12

  ret


#endif

/*
 * Double subtraction in PMNS without coefficients reduction
 * Operation: c [reg_p3] = c [reg_p3] - a [reg_p1] - b [reg_p2]
 */
.global fmt(mp_dblsub736x2_pmns)
fmt(mp_dblsub736x2_pmns):
  push  r12

  mov   r8, [reg_p3]
  mov   r9, [reg_p3+0x8]
  mov   r10, [reg_p3+0x10]
  mov   r11, [reg_p3+0x18]
  mov   r12, [reg_p3+0x20]
  mov   rcx, [reg_p3+0x28]

  sub   r8, [reg_p1]
  sbb   r9, [reg_p1+0x8]
  sbb   r10, [reg_p1+0x10]
  sbb   r11, [reg_p1+0x18]
  sbb   r12, [reg_p1+0x20]
  sbb   rcx, [reg_p1+0x28]

  sub   r8, [reg_p2]
  sbb   r9, [reg_p2+0x8]
  sbb   r10, [reg_p2+0x10]
  sbb   r11, [reg_p2+0x18]
  sbb   r12, [reg_p2+0x20]
  sbb   rcx, [reg_p2+0x28]

  mov   [reg_p3], r8
  mov   [reg_p3+0x8], r9
  mov   [reg_p3+0x10], r10
  mov   [reg_p3+0x18], r11
  mov   [reg_p3+0x20], r12
  mov   [reg_p3+0x28], rcx

  mov   r8, [reg_p3+0x30]
  mov   r9, [reg_p3+0x38]
  mov   r10, [reg_p3+0x40]
  mov   r11, [reg_p3+0x48]
  mov   r12, [reg_p3+0x50]
  mov   rcx, [reg_p3+0x58]

  sub   r8, [reg_p1+0x30]
  sbb   r9, [reg_p1+0x38]
  sbb   r10, [reg_p1+0x40]
  sbb   r11, [reg_p1+0x48]
  sbb   r12, [reg_p1+0x50]
  sbb   rcx, [reg_p1+0x58]

  sub   r8, [reg_p2+0x30]
  sbb   r9, [reg_p2+0x38]
  sbb   r10, [reg_p2+0x40]
  sbb   r11, [reg_p2+0x48]
  sbb   r12, [reg_p2+0x50]
  sbb   rcx, [reg_p2+0x58]

  mov   [reg_p3+0x30], r8
  mov   [reg_p3+0x38], r9
  mov   [reg_p3+0x40], r10
  mov   [reg_p3+0x48], r11
  mov   [reg_p3+0x50], r12
  mov   [reg_p3+0x58], rcx

  mov   r8, [reg_p3+0x60]
  mov   r9, [reg_p3+0x68]
  mov   r10, [reg_p3+0x70]
  mov   r11, [reg_p3+0x78]
  mov   r12, [reg_p3+0x80]
  mov   rcx, [reg_p3+0x88]

  sub   r8, [reg_p1+0x60]
  sbb   r9, [reg_p1+0x68]
  sbb   r10, [reg_p1+0x70]
  sbb   r11, [reg_p1+0x78]
  sbb   r12, [reg_p1+0x80]
  sbb   rcx, [reg_p1+0x88]

  sub   r8, [reg_p2+0x60]
  sbb   r9, [reg_p2+0x68]
  sbb   r10, [reg_p2+0x70]
  sbb   r11, [reg_p2+0x78]
  sbb   r12, [reg_p2+0x80]
  sbb   rcx, [reg_p2+0x88]

  mov   [reg_p3+0x60], r8
  mov   [reg_p3+0x68], r9
  mov   [reg_p3+0x70], r10
  mov   [reg_p3+0x78], r11
  mov   [reg_p3+0x80], r12
  mov   [reg_p3+0x88], rcx

  mov   r8, [reg_p3+0x90]
  mov   r9, [reg_p3+0x98]
  mov   r10, [reg_p3+0xa0]
  mov   r11, [reg_p3+0xa8]
  mov   r12, [reg_p3+0xb0]
  mov   rcx, [reg_p3+0xb8]

  sub   r8, [reg_p1+0x90]
  sbb   r9, [reg_p1+0x98]
  sbb   r10, [reg_p1+0xa0]
  sbb   r11, [reg_p1+0xa8]
  sbb   r12, [reg_p1+0xb0]
  sbb   rcx, [reg_p1+0xb8]

  sub   r8, [reg_p2+0x90]
  sbb   r9, [reg_p2+0x98]
  sbb   r10, [reg_p2+0xa0]
  sbb   r11, [reg_p2+0xa8]
  sbb   r12, [reg_p2+0xb0]
  sbb   rcx, [reg_p2+0xb8]

  mov   [reg_p3+0x90], r8
  mov   [reg_p3+0x98], r9
  mov   [reg_p3+0xa0], r10
  mov   [reg_p3+0xa8], r11
  mov   [reg_p3+0xb0], r12
  mov   [reg_p3+0xb8], rcx

  pop   r12

  ret


/*
 * Substraction in PMNS when coeffs are on two words
 * Operation: c [reg_p2] = a [reg_p1] - b [reg_p2]
 */
.global fmt(mp_sub736x2_pmns)
fmt(mp_sub736x2_pmns):
  push  r12

  mov   r8, [reg_p1]
  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]
  mov   r11, [reg_p1+0x18]
  mov   r12, [reg_p1+0x20]
  mov   rcx, [reg_p1+0x28]

  add   r8, [rip+fmt(pmns_subx2_cst0)]
  adc   r9, [rip+fmt(pmns_subx2_cst0)+0x8]
  adc   r10, [rip+fmt(pmns_subx2_cst0)+0x10]
  adc   r11, [rip+fmt(pmns_subx2_cst0)+0x18]
  adc   r12, [rip+fmt(pmns_subx2_cst0)+0x20]
  adc   rcx, [rip+fmt(pmns_subx2_cst0)+0x28]

  sub   r8, [reg_p2]
  sbb   r9, [reg_p2+0x8]
  sbb   r10, [reg_p2+0x10]
  sbb   r11, [reg_p2+0x18]
  sbb   r12, [reg_p2+0x20]
  sbb   rcx, [reg_p2+0x28]

  mov   [reg_p3], r8
  mov   [reg_p3+0x8], r9
  mov   [reg_p3+0x10], r10
  mov   [reg_p3+0x18], r11
  mov   [reg_p3+0x20], r12
  mov   [reg_p3+0x28], rcx

  mov   r8, [reg_p1+0x30]
  mov   r9, [reg_p1+0x38]
  mov   r10, [reg_p1+0x40]
  mov   r11, [reg_p1+0x48]
  mov   r12, [reg_p1+0x50]
  mov   rcx, [reg_p1+0x58]

  add   r8, [rip+fmt(pmns_subx2_cst1)]
  adc   r9, [rip+fmt(pmns_subx2_cst1)+0x8]
  adc   r10, [rip+fmt(pmns_subx2_cst1)+0x10]
  adc   r11, [rip+fmt(pmns_subx2_cst1)+0x18]
  adc   r12, [rip+fmt(pmns_subx2_cst1)+0x20]
  adc   rcx, [rip+fmt(pmns_subx2_cst1)+0x28]

  sub   r8, [reg_p2+0x30]
  sbb   r9, [reg_p2+0x38]
  sbb   r10, [reg_p2+0x40]
  sbb   r11, [reg_p2+0x48]
  sbb   r12, [reg_p2+0x50]
  sbb   rcx, [reg_p2+0x58]

  mov   [reg_p3+0x30], r8
  mov   [reg_p3+0x38], r9
  mov   [reg_p3+0x40], r10
  mov   [reg_p3+0x48], r11
  mov   [reg_p3+0x50], r12
  mov   [reg_p3+0x58], rcx

  mov   r8, [reg_p1+0x60]
  mov   r9, [reg_p1+0x68]
  mov   r10, [reg_p1+0x70]
  mov   r11, [reg_p1+0x78]
  mov   r12, [reg_p1+0x80]
  mov   rcx, [reg_p1+0x88]

  add   r8, [rip+fmt(pmns_subx2_cst1)]
  adc   r9, [rip+fmt(pmns_subx2_cst1)+0x8]
  adc   r10, [rip+fmt(pmns_subx2_cst1)+0x10]
  adc   r11, [rip+fmt(pmns_subx2_cst1)+0x18]
  adc   r12, [rip+fmt(pmns_subx2_cst1)+0x20]
  adc   rcx, [rip+fmt(pmns_subx2_cst1)+0x28]

  sub   r8, [reg_p2+0x60]
  sbb   r9, [reg_p2+0x68]
  sbb   r10, [reg_p2+0x70]
  sbb   r11, [reg_p2+0x78]
  sbb   r12, [reg_p2+0x80]
  sbb   rcx, [reg_p2+0x88]

  mov   [reg_p3+0x60], r8
  mov   [reg_p3+0x68], r9
  mov   [reg_p3+0x70], r10
  mov   [reg_p3+0x78], r11
  mov   [reg_p3+0x80], r12
  mov   [reg_p3+0x88], rcx

  mov   r8, [reg_p1+0x90]
  mov   r9, [reg_p1+0x98]
  mov   r10, [reg_p1+0xa0]
  mov   r11, [reg_p1+0xa8]
  mov   r12, [reg_p1+0xb0]
  mov   rcx, [reg_p1+0xb8]

  add   r8, [rip+fmt(pmns_subx2_cst1)]
  adc   r9, [rip+fmt(pmns_subx2_cst1)+0x8]
  adc   r10, [rip+fmt(pmns_subx2_cst1)+0x10]
  adc   r11, [rip+fmt(pmns_subx2_cst1)+0x18]
  adc   r12, [rip+fmt(pmns_subx2_cst1)+0x20]
  adc   rcx, [rip+fmt(pmns_subx2_cst1)+0x28]

  sub   r8, [reg_p2+0x90]
  sbb   r9, [reg_p2+0x98]
  sbb   r10, [reg_p2+0xa0]
  sbb   r11, [reg_p2+0xa8]
  sbb   r12, [reg_p2+0xb0]
  sbb   rcx, [reg_p2+0xb8]

  mov   [reg_p3+0x90], r8
  mov   [reg_p3+0x98], r9
  mov   [reg_p3+0xa0], r10
  mov   [reg_p3+0xa8], r11
  mov   [reg_p3+0xb0], r12
  mov   [reg_p3+0xb8], rcx

  pop   r12

  ret

/*
 * Addition in PMNS without coefficients reduction
 * Operation: c [reg_p3] = a [reg_p1] + b [reg_p2]
 */
.global fmt(mp_add)
fmt(mp_add):
  mov   r8, [reg_p1]
  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]

  add   r8, [reg_p2]
  adc   r9, [reg_p2+0x8]
  adc   r10, [reg_p2+0x10]

  mov   [reg_p3], r8
  mov   [reg_p3+0x8], r9
  mov   [reg_p3+0x10], r10

  mov   r8, [reg_p1+0x18]
  mov   r9, [reg_p1+0x20]
  mov   r10, [reg_p1+0x28]

  add   r8, [reg_p2+0x18]
  adc   r9, [reg_p2+0x20]
  adc   r10, [reg_p2+0x28]

  mov   [reg_p3+0x18], r8
  mov   [reg_p3+0x20], r9
  mov   [reg_p3+0x28], r10

  mov   r8, [reg_p1+0x30]
  mov   r9, [reg_p1+0x38]
  mov   r10, [reg_p1+0x40]

  add   r8, [reg_p2+0x30]
  adc   r9, [reg_p2+0x38]
  adc   r10, [reg_p2+0x40]

  mov   [reg_p3+0x30], r8
  mov   [reg_p3+0x38], r9
  mov   [reg_p3+0x40], r10

  mov   r8, [reg_p1+0x48]
  mov   r9, [reg_p1+0x50]
  mov   r10, [reg_p1+0x58]

  add   r8, [reg_p2+0x48]
  adc   r9, [reg_p2+0x50]
  adc   r10, [reg_p2+0x58]

  mov   [reg_p3+0x48], r8
  mov   [reg_p3+0x50], r9
  mov   [reg_p3+0x58], r10

  ret
