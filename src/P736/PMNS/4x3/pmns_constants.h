#ifndef PMNS_CONSTANTS_H
#define PMNS_CONSTANTS_H

/* Alice's generator values {XPA0 + XPA1*i, XQA0 + XQA1*i, XRA0 + XRA1*i}
* in GF(p^2), expressed in PMNS--Montgomery representation.
*/
static const uint64_t A_gen[6*NWORDS_FIELD] = { 0x4fd8c16ce4c44d2f,
            0x17d0d97568b65815, 0x0014598091c9212c, 0x06ac4d2f6027b678,
            0x32c9e39cf348f966, 0x002f7447c865f6d1, 0xfd5b5076346cd582,
            0x16479152c95254bc, 0x015e04f0e7a12453, 0x96b099f4eb3319c4,
            0x6e4adf486d591ba8, 0x002279c34fc27b55, //XPA0
                                                0xd9dba3d5fbf41c3f,
            0xab83bd46f2cf67df, 0x0058f443e6bd8fd5, 0x841cbaac449e0245,
            0xbc0589da3c88d7f3, 0x00796d961823b6d9, 0xd770856f870de411,
            0x21b78d41ceb16d19, 0x004c5e22ff1d4c99, 0xd38b5a57bb666e19,
            0x18be31e705e428cd, 0x0017b9d46c72541b, //XPA1
                                                0xc8e7627fdccc6a9b,
            0x06d919622f211927, 0x0006d13f958470a5, 0x6808fe48cff2ba53,
            0xbc2a03eaa088b9d9, 0x00e43331cc17c75e, 0x1b76265e9d4be3f4,
            0x097e967bc4f265fa, 0x0025063dcbc4d687, 0xedc57dc43bd60ee8,
            0xa264cb8d14aeb948, 0x001f30887e6ca607, //XQA0
                                                0x89c537255896d64d,
            0x6fa6a174dd4571e0, 0x001a19d6b6a63c46, 0xc74d354db41a5847,
            0x51032a8308d53336, 0x007ced6760428f7f, 0xe60b0adbf038dc0c,
            0x9319273c62d5f174, 0x005f5049d49a941c, 0x7c24f5f64d88b5df,
            0x47bf5a2d9668f672, 0x00073f0642e914bb, //XQA1
                                                0x4aa0e47720c8f68b,
            0xf32d43c87cffa528, 0x00728a62887688e8, 0x8b5ea80c472ea032,
            0x995016196a6ce1ee, 0x015746a5dc367800, 0xdb264b74adf4ba60,
            0x6df5ceb22b75764f, 0x001715e532bf6496, 0x1097522bf3c5d493,
            0xcd241e3014b61c94, 0x0010b4c73d43a19c, //XRA0
                                                0xec9158a7ad6cc13c,
            0xa8c6592a01a2864e, 0x0054547c1a9ebdbf, 0xe1d2555e8125ce58,
            0xa87ef6ba21b210ba, 0x015abb943a767a9f, 0x08d4bc01b8d8ec3c,
            0x79119c13073bc262, 0x00347818ea41a822, 0x1ea6fd809a5f7b38,
            0xc56ab187a959bbbc, 0x00137b8b7b58f6bb }; //XRA1

/* Bob's generator values {XPB0, XQB0, XRB0 + XRB1*i} in GF(p^2),
* expressed in PMNS--Montgomery representation.
*/
static const uint64_t B_gen[6*NWORDS_FIELD] = { 0x5d0dd9aa4d602876,
            0xb71e4c3de7ff8f95, 0x001478abf20ffdea, 0xb7ae559f317f7816,
            0xe6296b51f904b9b1, 0x004ffbdd291ff9a2, 0x44f7ea5ac904f75c,
            0xd7e66503c591d0a5, 0x00c474e7e20087ff, 0x062aeec44f10c665,
            0x63ac1d827971645a, 0x000b23410e50a523, //XPB0
                                                0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, //XPB1
                                                0xb70f3b828d88f85d,
            0xa7859bf8f8c758e0, 0x002d47f6b5e07313, 0xf6896f728a5c5bff,
            0x2d3d33692d863810, 0x00d9ec1044aecc18, 0x5b32bf0bea6b3331,
            0xb54170f23a727b1f, 0x0106096d842efc11, 0xabbc02fafdfba123,
            0x8367c4113ef33cf7, 0x000365f8e40848ce, //XQB0
                                                0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, //XQB1
                                                0xb1a9ad07a82c0988,
            0xdb2f05b66dbacb3e, 0x0147e54b58eef430, 0xc405d9b1a1e57fc5,
            0x27070429f10f11ad, 0x0150acb74b859d1b, 0x9361172f547694b8,
            0x13fb2642dae4b655, 0x0058ce34ea2543e6, 0xfa8f784047c75883,
            0x6745b20954f2b430, 0x0026f4bac3bd3ac1, //XRB0
                                                0x623f7310e56b8d16,
            0x3d4c179da9eda221, 0x0108c8ecd888ccc5, 0xad22916b97b04d02,
            0xc0d603d135befc97, 0x0100f0c9265771f3, 0xe31e4f31e651c16b,
            0x94a3c880c0b957bc, 0x00266664d2f65564, 0xe2f7d63df84875b8,
            0xd646c83ec4d82953, 0x00256918b59e1052 }; //XRB1

/* Montgomery constant Montgomery_R2 = (2^192)^2 in PMNS, i.e.,
* 2^192 in PMNS--Montgomery representation
*/
static const uint64_t Montgomery_R2[NWORDS_FIELD] = { 0x0000000000000000,
            0x4729569f38000000, 0x00c1eb9db7849fd4, 0xa26599e93228ccbb,
            0x1f23576415bc4756, 0x0054275d33774128, 0x0000000000007dc0,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000 };

/* Value one in PMNS--Montgomery representation */
static const uint64_t Montgomery_one[NWORDS_FIELD] = { 0x0000000000000000,
            0x5375588df8000000, 0x009a1dcd637f896d, 0x00000000000000b3,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000 };

#endif /* PMNS_CONSTANTS_H */
