#include "../../P736_internal.h"

#define KARATSUBA /* use KARATSUBA in mp_mul, if assembly version is not used */

#define fpcopy                        fpcopy736
#define fpadd                         fpadd736
#define fpsub                         fpsub736
#define fpneg                         fpneg736
#define fpdiv2                        fpdiv2_736
#define fpcorrection                  fpcorrection736
#define p                             p736
#define from_PMNS                     from_PMNS_736
#define to_PMNS                       to_PMNS_736
#define mp_dblsubx2_pmns              mp_dblsub736x2_pmns
#define mp_subx2_pmns                 mp_sub736x2_pmns

#include "fp_pmns_generic.c"
