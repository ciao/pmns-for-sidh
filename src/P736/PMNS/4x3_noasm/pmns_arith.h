#ifndef PMNS_ARITH_H
#define PMNS_ARITH_H

const digit_t PMNS_GAMMA[NWORDS_COEFF] = { 0x0000000000000000,
            0xa6dd0539d8000000, 0x016d430d277d246a }; /* 2^91*3^59 */
const digit_t PMNS_GAMMA_OVER_E[NWORDS_COEFF] = { 0x0000000000000000,
            0x54dba0a73b000000, 0x002da861a4efa48d }; /* 2^88*3^59 */
const digit_t PMNS_GAMMA_OVER_2E[NWORDS_COEFF] = { 0x0000000000000000,
            0xaa6dd0539d800000, 0x0016d430d277d246 }; /* 2^87*3^59 */
const digit_t PMNS_GAMMA2_OVER_E_MOD[NWORDS_COEFF] = { 0x0000000000000000,
            0x0000000000000000, 0x3cc8000000000000 }; /* gamma^2/e modulo 2^192 */

const digit_t pmns_corrections[8*NWORDS_COEFF] = { 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x5922fac628000000, 0x0092bcf2d882db95, 0x0000000000000000,
            0xb245f58c50000000, 0x012579e5b105b72a, 0x0000000000000000,
            0x648beb18a0000000, 0x004af3cb620b6e55, 0x0000000000000000,
            0xbdaee5dec8000000, 0x00ddb0be3a8e49ea, 0x0000000000000000,
            0x6ff4db6b18000000, 0x00032aa3eb940115, 0x0000000000000000,
            0xc917d63140000000, 0x0095e796c416dcaa, 0x0000000000000000,
            0x223ad0f768000000, 0x0128a4899c99b840 };
const digit_t pmns_carries[8] = { 0x0000000000000000,
            0x0000000000000001, 0x0000000000000002, 0x0000000000000004,
            0x0000000000000005, 0x0000000000000007, 0x0000000000000008,
            0x0000000000000009 };

const digit_t pmns_sub_cst0[NWORDS_COEFF] = { 0xffffffffffffffe8,
            0xf4970fad87ffffff, 0x0447c92776776d3f };
const digit_t pmns_sub_cst1[NWORDS_COEFF] = { 0xfffffffffffffffd,
            0xf4970fad87ffffff, 0x0447c92776776d3f };
const digit_t pmns_sub_cst1p1[NWORDS_COEFF] = { 0xfffffffffffffffe,
            0xf4970fad87ffffff, 0x0447c92776776d3f };
const digit_t pmns_K_minus_ep1[NWORDS_COEFF] = { 0xfffffffffffffff7,
            0xffffffffffffffff, 0x03ffffffffffffff };

const digit_t pmns_subx2_cst0[2*NWORDS_COEFF] = { 0x95d9a6616cdd3450,
            0x53729cadaea43b8a, 0x00be1fd808a2f84f, 0x0000000000000000,
            0x0000000000000000, 0x0200000000000000 };
const digit_t pmns_subx2_cst1[2*NWORDS_COEFF] = { 0x52bb34cc2d9ba68a,
            0x0818fbbf13d48771, 0xd0a655f5318c6d50, 0x0000000000000009,
            0x0000000000000000, 0x0200000000000000 };

#endif /* PMNS_ARITH_H */
