#ifndef PMNS_INTERNAL_H
#define PMNS_INTERNAL_H

#define NCOEFFS_FIELD 4 /* Number of PMNS coeffs */
#define NWORDS_COEFF 3  /* Number of words of one PMNS coeff */
#define PMNS_E 8
/* Upper bound on the coefficient size:
*    a coeff c is said to be reduced iif c < 2^K
*/
#define PMNS_K 186
#define PMNS_K_last_digit_shift 58
#define PMNS_K_last_digit_mask 0x03ffffffffffffff

#define post_rdc_mont_coeff_reduc_case1 pmns_coeff_reduc
#define post_rdc_mont_coeff_reduc_case2 pmns_coeff_reduc

#endif /* PMNS_INTERNAL_H */
