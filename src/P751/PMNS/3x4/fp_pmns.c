#include "../../P751_internal.h"

#define KARATSUBA /* use KARATSUBA in mp_mul, if assembly version is not used */

#define fpcopy                        fpcopy751
#define fpadd                         fpadd751
#define fpsub                         fpsub751
#define fpneg                         fpneg751
#define fpdiv2                        fpdiv2_751
#define fpcorrection                  fpcorrection751
#define p                             p751
#define from_PMNS                     from_PMNS_751
#define to_PMNS                       to_PMNS_751
#define mp_dblsubx2_pmns              mp_dblsub751x2_pmns
#define mp_subx2_pmns                 mp_sub751x2_pmns

#define _NO_FPADD /* implemented in assembly */
#define _NO_MP_MUL /* implemented in assembly */
#define _NO_RDC_MONT /* implemented in assembly */

#include "fp_pmns_generic.c"
