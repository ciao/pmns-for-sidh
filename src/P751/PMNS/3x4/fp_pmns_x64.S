/*
 * SIDH: an efficient supersingular isogeny cryptography library
 * x64 assembly code for P751 using PMNS
 */

.intel_syntax noprefix

/* Format function and variable names for Mac OS X */
#if defined(__APPLE__)
  #define fmt(f) _##f
#else
  #define fmt(f) f
#endif

/* Registers that are used for parameter passing: */
#define reg_p1 rdi
#define reg_p2 rsi
#define reg_p3 rdx

#include "pmns_internal.h"

.text
/*
 * Addition in GF(p) in PMNS
 * Operation: c [reg_p2] = a [reg_p1] + b [reg_p2]
 */
.global fmt(fpadd751)
fmt(fpadd751):
  push  rbx
  push  r12
  push  r13

  mov   rax, (PMNS_K_last_digit_mask >> 1)
  lea   rbx, [rip+fmt(pmns_corrections)]
  lea   rcx, [rip+fmt(pmns_carries)]

  mov   r8, [reg_p1]
  mov   r9, [reg_p1+0x8]
  mov   r10, [reg_p1+0x10]
  mov   r11, [reg_p1+0x18]

  add   r8, [reg_p2]
  adc   r9, [reg_p2+0x8]
  adc   r10, [reg_p2+0x10]
  adc   r11, [reg_p2+0x18]
  mov   r12, r11
  and   r11, rax
  shr   r12, PMNS_K_last_digit_shift-1
  shl   r12, 2
  add   r9, [rbx+8*r12+0x8]
  adc   r10, [rbx+8*r12+0x10]
  adc   r11, [rbx+8*r12+0x18]

  mov   [reg_p3], r8
  mov   [reg_p3+0x8], r9
  mov   [reg_p3+0x10], r10
  mov   [reg_p3+0x18], r11

  mov   r8, [reg_p1+0x20]
  mov   r9, [reg_p1+0x28]
  mov   r10, [reg_p1+0x30]
  mov   r11, [reg_p1+0x38]

  add   r8, [reg_p2+0x20]
  adc   r9, [reg_p2+0x28]
  adc   r10, [reg_p2+0x30]
  adc   r11, [reg_p2+0x38]
  mov   r13, r11
  and   r11, rax
  shr   r13, PMNS_K_last_digit_shift-1
  shl   r13, 2
  add   r8, [rcx+2*r12]
  adc   r9, [rbx+8*r13+0x8]
  adc   r10, [rbx+8*r13+0x10]
  adc   r11, [rbx+8*r13+0x18]

  mov   [reg_p3+0x20], r8
  mov   [reg_p3+0x28], r9
  mov   [reg_p3+0x30], r10
  mov   [reg_p3+0x38], r11

  mov   r8, [reg_p1+0x40]
  mov   r9, [reg_p1+0x48]
  mov   r10, [reg_p1+0x50]
  mov   r11, [reg_p1+0x58]

  add   r8, [reg_p2+0x40]
  adc   r9, [reg_p2+0x48]
  adc   r10, [reg_p2+0x50]
  adc   r11, [reg_p2+0x58]
  mov   r12, r11
  and   r11, rax
  shr   r12, PMNS_K_last_digit_shift-1
  shl   r12, 2
  add   r8, [rcx+2*r13]
  adc   r9, [rbx+8*r12+0x8]
  adc   r10, [rbx+8*r12+0x10]
  adc   r11, [rbx+8*r12+0x18]

  mov   [reg_p3+0x40], r8
  mov   [reg_p3+0x48], r9
  mov   [reg_p3+0x50], r10
  mov   [reg_p3+0x58], r11

  mov   r8, [reg_p3]
  mov   r9, [reg_p3+0x8]
  mov   r10, [reg_p3+0x10]
  mov   r11, [reg_p3+0x18]
  mov   r13, [rcx+2*r12]
  lea   r13, [r13 + r13*2]
  add   r8, r13
  adc   r9, 0
  adc   r10, 0
  adc   r11, 0

  mov   [reg_p3], r8
  mov   [reg_p3+0x8], r9
  mov   [reg_p3+0x10], r10
  mov   [reg_p3+0x18], r11

  pop   r13
  pop   r12
  pop   rbx

  ret

/*
 * Schoolbook multiplication for integers of 192 bits.
 * rdx is reserved for internal usage, registers parameters must not be rdx.
 * rax is reserved for internal usage, registers parameters must not be rax.
 * The result is written in R0, R1, R2, R3, R4 and rax.
 */
.macro MUL256_SCHOOL M0, M1, C, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9
    mov    rdx, \M0
    mulx   \T0, \T1, \M1     // T0:T1 = A0*B0
    mov    \C, \T1           // C0_final
    mulx   \T1, \T2, 8\M1    // T1:T2 = A0*B1
    xor    rax, rax
    adox   \T0, \T2
    mulx   \T2, \T3, 16\M1   // T2:T3 = A0*B2
    adox   \T1, \T3
    mulx   \T3, \T4, 24\M1   // T3:T4 = A0*B3
    adox   \T2, \T4

    mov    rdx, 8\M0
    mulx   \T5, \T4, \M1     // T5:T4 = A1*B0
    adox   \T3, rax
    xor    rax, rax
    mulx   \T6, \T7, 8\M1    // T6:T7 = A1*B1
    adox   \T4, \T0
    mov    8\C, \T4          // C1_final
    adcx   \T5, \T7
    mulx   \T7, \T8, 16\M1   // T7:T8 = A1*B2
    adcx   \T6, \T8
    adox   \T5, \T1
    mulx   \T8, \T9, 24\M1   // T8:T9 = A1*B3
    adcx   \T7, \T9
    adcx   \T8, rax
    adox   \T6, \T2

    mov    rdx, 16\M0
    mulx   \T1, \T0, \M1     // T1:T0 = A2*B0
    adox   \T7, \T3
    adox   \T8, rax
    xor    rax, rax
    mulx   \T2, \T3, 8\M1    // T2:T3 = A2*B1
    adox   \T0, \T5
    mov    16\C, \T0         // C2_final
    adcx   \T1, \T3
    mulx   \T3, \T4, 16\M1   // T3:T4 = A2*B2
    adcx   \T2, \T4
    adox   \T1, \T6
    mulx   \T4,\T9, 24\M1    // T3:T4 = A2*B3
    adcx   \T3, \T9
    mov    rdx, 24\M0
    adcx   \T4, rax

    adox   \T2, \T7
    adox   \T3, \T8
    adox   \T4, rax

    mulx   \T5, \T0, \M1     // T5:T0 = A3*B0
    xor    rax, rax
    mulx   \T6, \T7, 8\M1    // T6:T7 = A3*B1
    adcx   \T5, \T7
    adox   \T1, \T0
    mulx   \T7, \T8, 16\M1   // T7:T8 = A3*B2
    adcx   \T6, \T8
    adox   \T2, \T5
    mulx   \T8, \T9, 24\M1   // T8:T9 = A3*B3
    adcx   \T7, \T9
    adcx   \T8, rax

    adox   \T3, \T6
    adox   \T4, \T7
    adox   \T8, rax
.endm

/*
 * Input is in registers A0, A1, A2, A3 and in immediate constants I0 and I1
 * Output is written in R0, R1, R2, R3, R4, R5
 * All registers must be different.
 * rdx is reserved for internal usage, registers parameters must not be rdx.
 * rax is reserved for internal usage, registers parameters must not be rax.
 */
.macro MUL256x128imm_SCHOOL A0, A1, A2, A3, R0, R1, R2, R3, R4, R5, T0, I0, I1
  mov   rdx, \I0
  mulx  \R1, \R0, \A0       /* R1:R0 = A1*I0 */     /* R0 final */
  xor   rax, rax
  mulx  \R2, \R4, \A1       /* R2:R4 = A1*I0 */
  mulx  \R3, \R5, \A2       /* R3:R5 = A2*I0 */
  adox  \R1, \R4
  adox  \R2, \R5
  mulx  \R4, \R5, \A3       /* R4:R5 = A3*I0 */
  adox  \R3, \R5
  adox  \R4, rax

  xor   rax, rax
  mov   rdx, \I1
  mulx  \T0, \R5, \A0       /* T0:R5 = A0*I1 */
  adcx  \R1, \R5                                    /* R1 final */
  adcx  \R2, \T0
  mulx  \R5, \T0, \A1       /* R5:T0 = A1*I1 */
  adcx  \R3, \R5
  adox  \R2, \T0                                    /* R2 final */
  mulx  \R5, \T0, \A2       /* R5:T0 = A2*I1 */
  adcx  \R4, \R5
  adox  \R3, \T0                                    /* R3 final */
  mulx  \R5, \T0, \A3       /* R5:T0 = A3*I1 */
  adcx  \R5, rax
  adox  \R4, \T0                                    /* R4 final */
  adox  \R5, rax                                    /* R5 final */
.endm

/*
 * Input is in registers A0, A1, A2, A3 and in immediate constants I0, I1 and I2
 * Output is written in R0, R1, R2, R3, R4, R5, R6
 * All registers must be different.
 * Input registers may be overwritten.
 * rdx is reserved for internal usage, registers parameters must not be rdx.
 * rax is reserved for internal usage, registers parameters must not be rax.
 */
.macro MUL256x192imm_SCHOOL A0, A1, A2, A3, R0, R1, R2, R3, R4, R5, R6, I0, I1, I2
  mov   rdx, \I0
  mulx  \R1, \R0, \A0       /* R1:R0 = A1*I0 */     /* R0 final */
  xor   rax, rax
  mulx  \R2, \R4, \A1       /* R2:R4 = A1*I0 */
  mulx  \R3, \R5, \A2       /* R3:R5 = A2*I0 */
  adox  \R1, \R4
  adox  \R2, \R5
  mulx  \R4, \R5, \A3       /* R4:R5 = A3*I0 */
  adox  \R3, \R5
  adox  \R4, rax

  xor   rax, rax
  mov   rdx, \I1
  mulx  \R6, \R5, \A0       /* R6:R5 = A0*I1 */
  adcx  \R1, \R5
  adcx  \R2, \R6
  mulx  \R5, \R6, \A1       /* R5:R6 = A1*I1 */
  adcx  \R3, \R5
  adox  \R2, \R6
  mulx  \R5, \R6, \A2       /* R5:R6 = A2*I1 */
  adcx  \R4, \R5
  adox  \R3, \R6
  mulx  \R5, \R6, \A3       /* R5:R6 = A3*I1 */
  adcx  \R5, rax
  adox  \R4, \R6
  adox  \R5, rax

  xor   rax, rax
  mov   rdx, \I2
  mulx  \R6, \A0, \A0       /* R6:A0 = A0*I2 */
  adcx  \R2, \A0
  adcx  \R3, \R6
  mulx  \R6, \A0, \A1       /* R6:A0 = A1*I2 */
  adcx  \R4, \R6
  adox  \R3, \A0
  mulx  \R6, \A0, \A2       /* R6:A0 = A2*I2 */
  adcx  \R5, \R6
  adox  \R4, \A0
  mulx  \R6, \A0, \A3       /* R5:R6 = A3*I2 */
  adcx  \R6, rax
  adox  \R5, \A0
  adox  \R6, rax
.endm

/*
 * Montgomery reduction in PMNS
 * Operation: c [reg_p2] = redc(a [reg_p1])
 */
.global fmt(rdc_mont)
fmt(rdc_mont):
  push  r12
  push  r13
  push  r14
  push  r15
  push  rbx
  push  rbp

  mov   r8, [reg_p1+0x80]
  mov   r9, [reg_p1+0x88]
  mov   r10, [reg_p1+0x90]
  mov   r11, [reg_p1+0x98]

  mov   r12, [reg_p1]
  mov   r13, [reg_p1+0x08]
  mov   r14, [reg_p1+0x10]

  mov   rdx, 0xb000000000000000
  mulx  rbx, r15, r12
  add   r9, r15
  adc   r10, rbx
  adc   r11, 0

  mulx  rbp, r15, r13
  add   r10, r15
  adc   r11, rbp

  mulx  rbx, r15, r14
  add   r11, r15

  mov   rdx, 0xebef8c87d897df16
  mulx  rbp, r15, r12
  add   r10, r15
  adc   r11, rbp

  mulx  rbx, r15, r13
  add   r11, r15

  mov   rdx, 0x02510fb4fd908363
  mulx  rbp, r15, r12
  add   r11, r15

  mov   r12, [reg_p1+0x40]

  mov   rdx, 0x2b00000000000000
  mulx  r14, r13, r12
  add   r11, r13

  MUL256x128imm_SCHOOL r8, r9, r10, r11, rbx, rcx, r12, r13, r14, r15, rbp, 0xc3cea59789c79d44, 0x06f32f1ef8b18a2b

  shld  rax, r11, 60 /* rax is zero from MUL256x128imm_SCHOOL */
  shld  r11, r10, 60
  shld  r10, r9, 60
  shld  r9, r8, 60
  shl   r8, 60

  add   rbx, r9
  adc   rcx, r10
  adc   r12, r11
  adc   r13, rax
  adc   r14, 0
  adc   r15, 0

  mov   r9, [reg_p1+0x40]
  add   r8, [reg_p1+0x48]
  adc   rbx, [reg_p1+0x50]
  adc   rcx, [reg_p1+0x58]
  adc   r12, [reg_p1+0x60]
  adc   r13, [reg_p1+0x68]
  adc   r14, [reg_p1+0x70]
  adc   r15, [reg_p1+0x78]

  mov   [reg_p2+0x20], r12
  mov   [reg_p2+0x28], r13
  mov   [reg_p2+0x30], r14
  mov   [reg_p2+0x38], r15

  MUL256x128imm_SCHOOL r9, r8, rbx, rcx, r10, r11, r12, r13, r14, r15, rbp, 0xc3cea59789c79d44, 0x06f32f1ef8b18a2b

  shld  rax, rcx, 60 /* rax is zero from MUL256x128imm_SCHOOL */
  shld  rcx, rbx, 60
  shld  rbx, r8, 60
  shld  r8, r9, 60
  shl   r9, 60

  add   r10, r8
  adc   r11, rbx
  adc   r12, rcx
  adc   r13, rax
  adc   r14, 0
  adc   r15, 0

  mov   r8, [reg_p1]
  add   r9, [reg_p1+0x08]
  adc   r10, [reg_p1+0x10]
  adc   r11, [reg_p1+0x18]
  adc   r12, [reg_p1+0x20]
  adc   r13, [reg_p1+0x28]
  adc   r14, [reg_p1+0x30]
  adc   r15, [reg_p1+0x38]

  mov   [reg_p2], r12
  mov   [reg_p2+0x08], r13
  mov   [reg_p2+0x10], r14
  mov   [reg_p2+0x18], r15

  MUL256x192imm_SCHOOL r8, r9, r10, r11, r12, r13, r14, r15, rbx, rcx, rbp, 0xb000000000000000, 0xebef8c87d897df16, 0x02510fb4fd908363

  add   r12, [reg_p1+0x88]
  adc   r13, [reg_p1+0x90]
  adc   r14, [reg_p1+0x98]
  adc   r15, [reg_p1+0xa0]
  adc   rbx, [reg_p1+0xa8]
  adc   rcx, [reg_p1+0xb0]
  adc   rbp, [reg_p1+0xb8]

  mov   [reg_p2+0x40], r15
  mov   [reg_p2+0x48], rbx
  mov   [reg_p2+0x50], rcx
  mov   [reg_p2+0x58], rbp

  pop  rbp
  pop  rbx
  pop  r15
  pop  r14
  pop  r13
  pop  r12

  ret

/*
 * Multiplication in GF(p) in PMNS
 * Operation: c [reg_p3] = a [reg_p1] * b [reg_p2]
 */
.global fmt(mp_mul)
fmt(mp_mul):
  push  r12
  push  r13
  push  r14
  push  r15
  push  rbx
  push  rbp

  mov   rcx, reg_p3
  sub   rsp, 0x18

  MUL256_SCHOOL [reg_p1], [reg_p2], [rcx], r8, r9, r10, r11, r12, r13, r14, r15, rbx, rbp

  mov   [rcx+0x18], r9
  mov   [rcx+0x20], r10
  mov   [rcx+0x28], r11
  mov   [rcx+0x30], r12
  mov   [rcx+0x38], rbx

  MUL256_SCHOOL [reg_p1+0x20], [reg_p2], [rcx+0x40], r8, r9, r10, r11, r12, r13, r14, r15, rbx, rbp

  mov   [rcx+0x58], r9
  mov   [rcx+0x60], r10
  mov   [rcx+0x68], r11
  mov   [rcx+0x70], r12
  mov   [rcx+0x78], rbx

  MUL256_SCHOOL [reg_p1+0x40], [reg_p2], [rcx+0x80], r8, r9, r10, r11, r12, r13, r14, r15, rbx, rbp

  mov   [rcx+0x98], r9
  mov   [rcx+0xa0], r10
  mov   [rcx+0xa8], r11
  mov   [rcx+0xb0], r12
  mov   [rcx+0xb8], rbx

  MUL256_SCHOOL [reg_p1], [reg_p2+0x20], [rsp], r8, r9, r10, r11, r12, r13, r14, r15, rbx, rbp

  mov   r13, [rsp]
  mov   r14, [rsp+0x08]
  mov   r15, [rsp+0x10]

  add   [rcx+0x40], r13
  adc   [rcx+0x48], r14
  adc   [rcx+0x50], r15
  adc   [rcx+0x58], r9
  adc   [rcx+0x60], r10
  adc   [rcx+0x68], r11
  adc   [rcx+0x70], r12
  adc   [rcx+0x78], rbx

  MUL256_SCHOOL [reg_p1+0x20], [reg_p2+0x20], [rsp], r8, r9, r10, r11, r12, r13, r14, r15, rbx, rbp

  mov   r13, [rsp]
  mov   r14, [rsp+0x08]
  mov   r15, [rsp+0x10]

  add   [rcx+0x80], r13
  adc   [rcx+0x88], r14
  adc   [rcx+0x90], r15
  adc   [rcx+0x98], r9
  adc   [rcx+0xa0], r10
  adc   [rcx+0xa8], r11
  adc   [rcx+0xb0], r12
  adc   [rcx+0xb8], rbx

  MUL256_SCHOOL [reg_p1+0x40], [reg_p2+0x20], [rsp], r8, r9, r10, r11, r12, r13, r14, r15, rbx, rbp

  mov   r13, [rsp]
  mov   r14, [rsp+0x08]
  mov   r15, [rsp+0x10]

  add   [rcx], r13
  adc   [rcx+0x08], r14
  adc   [rcx+0x10], r15
  adc   [rcx+0x18], r9
  adc   [rcx+0x20], r10
  adc   [rcx+0x28], r11
  adc   [rcx+0x30], r12
  adc   [rcx+0x38], rbx
  shld  rbx, r12, 1
  shld  r12, r11, 1
  shld  r11, r10, 1
  shld  r10, r9, 1
  shld  r9, r15, 1
  shld  r15, r14, 1
  shld  r14, r13, 1
  shl   r13, 1
  add   [rcx], r13
  adc   [rcx+0x08], r14
  adc   [rcx+0x10], r15
  adc   [rcx+0x18], r9
  adc   [rcx+0x20], r10
  adc   [rcx+0x28], r11
  adc   [rcx+0x30], r12
  adc   [rcx+0x38], rbx

  MUL256_SCHOOL [reg_p1], [reg_p2+0x40], [rsp], r8, r9, r10, r11, r12, r13, r14, r15, rbx, rbp

  mov   r13, [rsp]
  mov   r14, [rsp+0x08]
  mov   r15, [rsp+0x10]

  add   [rcx+0x80], r13
  adc   [rcx+0x88], r14
  adc   [rcx+0x90], r15
  adc   [rcx+0x98], r9
  adc   [rcx+0xa0], r10
  adc   [rcx+0xa8], r11
  adc   [rcx+0xb0], r12
  adc   [rcx+0xb8], rbx

  MUL256_SCHOOL [reg_p1+0x20], [reg_p2+0x40], [rsp], r8, r9, r10, r11, r12, r13, r14, r15, rbx, rbp

  mov   r13, [rsp]
  mov   r14, [rsp+0x08]
  mov   r15, [rsp+0x10]

  add   [rcx], r13
  adc   [rcx+0x08], r14
  adc   [rcx+0x10], r15
  adc   [rcx+0x18], r9
  adc   [rcx+0x20], r10
  adc   [rcx+0x28], r11
  adc   [rcx+0x30], r12
  adc   [rcx+0x38], rbx
  shld  rbx, r12, 1
  shld  r12, r11, 1
  shld  r11, r10, 1
  shld  r10, r9, 1
  shld  r9, r15, 1
  shld  r15, r14, 1
  shld  r14, r13, 1
  shl   r13, 1
  add   [rcx], r13
  adc   [rcx+0x08], r14
  adc   [rcx+0x10], r15
  adc   [rcx+0x18], r9
  adc   [rcx+0x20], r10
  adc   [rcx+0x28], r11
  adc   [rcx+0x30], r12
  adc   [rcx+0x38], rbx

  MUL256_SCHOOL [reg_p1+0x40], [reg_p2+0x40], [rsp], r8, r9, r10, r11, r12, r13, r14, r15, rbx, rbp

  mov   r13, [rsp]
  mov   r14, [rsp+0x08]
  mov   r15, [rsp+0x10]

  add   [rcx+0x40], r13
  adc   [rcx+0x48], r14
  adc   [rcx+0x50], r15
  adc   [rcx+0x58], r9
  adc   [rcx+0x60], r10
  adc   [rcx+0x68], r11
  adc   [rcx+0x70], r12
  adc   [rcx+0x78], rbx
  shld  rbx, r12, 1
  shld  r12, r11, 1
  shld  r11, r10, 1
  shld  r10, r9, 1
  shld  r9, r15, 1
  shld  r15, r14, 1
  shld  r14, r13, 1
  shl   r13, 1
  add   [rcx+0x40], r13
  adc   [rcx+0x48], r14
  adc   [rcx+0x50], r15
  adc   [rcx+0x58], r9
  adc   [rcx+0x60], r10
  adc   [rcx+0x68], r11
  adc   [rcx+0x70], r12
  adc   [rcx+0x78], rbx

  add   rsp, 0x18

  pop  rbp
  pop  rbx
  pop  r15
  pop  r14
  pop  r13
  pop  r12

  ret
