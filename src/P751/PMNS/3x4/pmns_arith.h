#ifndef PMNS_ARITH_H
#define PMNS_ARITH_H

const digit_t PMNS_GAMMA[NWORDS_COEFF] = { 0x0000000000000000,
            0x1000000000000000, 0xc3cea59789c79d44, 0x06f32f1ef8b18a2b }; /* 2^124*3^80 */
const digit_t PMNS_GAMMA_OVER_E[NWORDS_COEFF] = { 0x0000000000000000,
            0xb000000000000000, 0xebef8c87d897df16, 0x02510fb4fd908363 }; /* 2^124*3^79 */
const digit_t PMNS_GAMMA_OVER_2E[NWORDS_COEFF] = { 0x0000000000000000,
            0x5800000000000000, 0xf5f7c643ec4bef8b, 0x012887da7ec841b1 }; /* 2^123*3^79 */
const digit_t PMNS_GAMMA2_OVER_E_MOD[NWORDS_COEFF] = { 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x2b00000000000000 }; /* gamma^2/e modulo 2^256 */

const digit_t pmns_corrections[8*NWORDS_COEFF] = { 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0xf000000000000000, 0x3c315a68763862bb,
            0x010cd0e1074e75d4, 0x0000000000000000, 0xe000000000000000,
            0x7862b4d0ec70c577, 0x0219a1c20e9ceba8, 0x0000000000000000,
            0xd000000000000000, 0xb4940f3962a92833, 0x032672a315eb617c,
            0x0000000000000000, 0xc000000000000000, 0xf0c569a1d8e18aef,
            0x043343841d39d750, 0x0000000000000000, 0xb000000000000000,
            0x2cf6c40a4f19edab, 0x0540146524884d25, 0x0000000000000000,
            0xa000000000000000, 0x69281e72c5525067, 0x064ce5462bd6c2f9,
            0x0000000000000000, 0x8000000000000000, 0xe18ad343b1c315df,
            0x006687083a73aea1 };
const digit_t pmns_carries[8] = { 0x0000000000000000,
            0x0000000000000001, 0x0000000000000002, 0x0000000000000003,
            0x0000000000000004, 0x0000000000000005, 0x0000000000000006,
            0x0000000000000008 };

const digit_t pmns_sub_cst0[NWORDS_COEFF] = { 0xfffffffffffffff7,
            0x2fffffffffffffff, 0x4b6bf0c69d56d7cc, 0x14d98d5cea149e83 };
const digit_t pmns_sub_cst1[NWORDS_COEFF] = { 0xfffffffffffffffd,
            0x2fffffffffffffff, 0x4b6bf0c69d56d7cc, 0x14d98d5cea149e83 };
const digit_t pmns_sub_cst1p1[NWORDS_COEFF] = { 0xfffffffffffffffe,
            0x2fffffffffffffff, 0x4b6bf0c69d56d7cc, 0x14d98d5cea149e83 };
const digit_t pmns_K_minus_ep1[NWORDS_COEFF] = { 0xfffffffffffffffc,
            0xffffffffffffffff, 0xffffffffffffffff, 0x0fffffffffffffff };

const digit_t pmns_subx2_cst0[2*NWORDS_COEFF] = { 0x6d392eeeb1a34580,
            0x69776e262746eae0, 0xbabfed3bfbacf630, 0x06229f1d40e29951,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0900000000000000 };
const digit_t pmns_subx2_cst1[2*NWORDS_COEFF] = { 0x246864fa3b366c80,
            0x2327cf620d17a3a0, 0xa0cdab1a4517d6e0, 0x9d29aab2788489c8,
            0x0000000000000002, 0x0000000000000000, 0x0000000000000000,
            0x0900000000000000 };

#endif /* PMNS_ARITH_H */
