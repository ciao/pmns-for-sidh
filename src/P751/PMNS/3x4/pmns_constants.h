#ifndef PMNS_CONSTANTS_H
#define PMNS_CONSTANTS_H

/* Alice's generator values {XPA0 + XPA1*i, XQA0 + XQA1*i, XRA0 + XRA1*i}
* in GF(p^2), expressed in PMNS--Montgomery representation.
*/
static const uint64_t A_gen[6*NWORDS_FIELD] = { 0xc95423df29e9c8c2,
            0x39d830882558baca, 0x80f4d55ad1b84c6f, 0x05b5adaf65094663,
            0x83de15b07419e8e0, 0x82748a18d73a428a, 0x0a747ff19dec6456,
            0x066099836b992c6d, 0x7ddbb18c27d4d8e7, 0xd6e773b1d5ecc2b4,
            0x5c2b9e59453f79b6, 0x015471c879fbfa0e, //XPA0
                                                0x65d22340c5ef1d1e,
            0x14cb330f3d61415c, 0x416b284e9c301eaa, 0x05cef64343981cd6,
            0xa13941dd49968cb4, 0x94a67c87b8180b90, 0x3197dc2f93548838,
            0x0177d1941f6c9b8b, 0xf5205e616bd80d28, 0xab31838a20b3d39b,
            0x9cb0b4bc5fbc2563, 0x00afb3595deca377, //XPA1
                                                0xcebb48a9423e0bc0,
            0x3574cb3d5560db99, 0x4ea47c58842a6f60, 0x031f5b159d0bc210,
            0x4f65c1660c47d152, 0xd3f1420e262683de, 0x7b3b0c6ddd8650ae,
            0x0636f869b64cd64c, 0x9e62967c47218850, 0x0354bf3ad06860bd,
            0x2366ed037db87f5b, 0x00b2b2733a885adf, //XQA0
                                                0xfb5e7942cb431ce3,
            0x1cc6712d9d23dd64, 0x282e4091c9160564, 0x01fd44a4837c73db,
            0xa343d4176977448b, 0xace0f7c2de6a58b2, 0x1a52d81cbe6da632,
            0x04e0decf264b8662, 0x64adb7c19f19a2d3, 0x6a496045dded4679,
            0x5f4b723d02e0dc08, 0x013bf8380ec27ea9, //XQA1
                                                0x95580a9131640c88,
            0xb768e00e7ac6fe9c, 0x62913ab80463f000, 0x050c77774407ebd3,
            0x895ff199c59d2987, 0x5eaf08078f3d8ee2, 0x3a0fca849c0bff89,
            0x06ef095792029714, 0xf77fe8aa41ef0f60, 0xa5023b2005578518,
            0x9191e4e4eaed273b, 0x007e42fab16b6cf5, //XRA0
                                                0x87eb694927783d33,
            0xf01085c4c44eb16f, 0xee150a8ec97d7cff, 0x05a9fdaf18e4ed20,
            0x7ad2718832ceb8a3, 0xb3a29834ea62c65b, 0x0695698aafdd10ee,
            0x01d9af28cfb75377, 0xf5b20cdce513b181, 0x7b04ee295f158d73,
            0x59abb5e10266feaf, 0x009a8a311fc96f69 }; //XRA1

/* Bob's generator values {XPB0, XQB0, XRB0 + XRB1*i} in GF(p^2),
* expressed in PMNS--Montgomery representation.
*/
static const uint64_t B_gen[6*NWORDS_FIELD] = { 0x59a69f9bc3eaa48d,
            0xdf133701c5a3ff78, 0x0c1ff7e4e0bac236, 0x036b244be5aa6196,
            0x12ecb5695551e3c5, 0x08e665017b376f7c, 0xeb5fb61d22f22807,
            0x0470d35fc0e718de, 0xb9c9f9c48b577a35, 0xcd4bc1eec509cd4d,
            0xb1e793ea5d82d282, 0x0235d6dc416eb5a6, //XPB0
                                                0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, //XPB1
                                                0x2a1c0b38457f51ef,
            0xaf789c44f986bdd8, 0x44129e847ae8764b, 0x00868decb2cbf0bd,
            0x947805a7d3eb535c, 0xd1893dc3d3e02ee6, 0xb6e640d27f5fdaee,
            0x03a9c8926f79b889, 0x85221cd8550777f6, 0x3fc831e625292320,
            0x56f9882188ab9ac0, 0x021ae8a00c6f9bb1, //XQB0
                                                0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, //XQB1
                                                0x084308e1db9ca42b,
            0x949d4d49fc8b4e6f, 0x84356f7290d18fd4, 0x0380d26d84110bd0,
            0x9bca50e715dfd77f, 0xd5c6e3374c2cdc37, 0x754dfa10d3388676,
            0x0528224dc942b073, 0x7420f955240f4739, 0xc5e4f7be7fef4543,
            0x23bf6507c261783e, 0x00497738c7d094a6, //XRB0
                                                0x9d0543f31d82ceda,
            0x57afc7ecd584a515, 0x3b83ea179c191e2e, 0x05addfa9971a7f2d,
            0xd0a450ca03d52559, 0x2791ee7faa6c36da, 0xd96d0d212d8c61cb,
            0x01b054481931840a, 0x3401a0633371e37b, 0xe260038b6d6c9f0c,
            0x9fb1a59ec3cdadf7, 0x02363bb5e09fa921 }; //XRB1

/* Montgomery constant Montgomery_R2 = (2^256)^2 in PMNS, i.e.,
* 2^256 in PMNS--Montgomery representation
*/
static const uint64_t Montgomery_R2[NWORDS_FIELD] = { 0x0000000000000000,
            0xe000000000000000, 0x51e352c460d8def0, 0x01b681d749a5b986,
            0x9f113987a49e8c72, 0x02b0118cc47c0aa6, 0x47f56dab9c293e22,
            0x059bf03ce741ede6, 0x000000000000054c, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000 };

/* Value one in PMNS--Montgomery representation */
static const uint64_t Montgomery_one[NWORDS_FIELD] = { 0x0000000000000000,
            0xc000000000000000, 0x76f0b6b09fede26d, 0x05cd5fa5070891d8,
            0x0000000000000024, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
            0x0000000000000000, 0x0000000000000000 };

#endif /* PMNS_CONSTANTS_H */
