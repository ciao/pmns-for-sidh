#if NWORDS_COEFF != 1
  #error "this file should only be included when NWORDS_COEFF is 1"
#endif

#include <gmp.h>

#include "pmns_arith.h"

extern const uint64_t p[NWORDS64_FIELD];

/* copy from config.h, but without input carry */
#define ADD(addend1, addend2, carryOut, sumOut)                         \
    { uint128_t tempReg = (uint128_t)(addend1) + (uint128_t)(addend2);  \
    (carryOut) = (digit_t)(tempReg >> RADIX);                           \
    (sumOut) = (digit_t)tempReg; }
/* copy from config.h, but without input borrow */
#define SUB(minuend, subtrahend, borrowOut, differenceOut)               \
    { uint128_t tempReg = (uint128_t)(minuend) - (uint128_t)(subtrahend); \
    (borrowOut) = (digit_t)(tempReg >> (sizeof(uint128_t)*8 - 1));        \
    (differenceOut) = (digit_t)tempReg; }

/* Conversion from a residue to PMNS representation by writing v in basis gamma
 * with digit in [0, gamma[.
 * Input: a in [0, p[
 * Output: coeffs of pa in [0, gamma[
 */
void
to_PMNS (const felm_t a, felm_t pa)
{
  digit_t c[NCOEFFS_FIELD] = {0};
  fpcopy (a, c);

  /* */
  unsigned nw = NCOEFFS_FIELD;
  for ( ; nw > 0 && c[nw-1] == 0; nw--);

  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
  {
    pa[i] = mpn_divmod_1 ((mp_ptr) c, (mp_srcptr) c, nw, PMNS_GAMMA);
    if (c[nw-1] == 0 && nw >= 2)
      nw -= 1;
  }
}

/* Conversion from a PMNS polynomial to a residue by evaluating the polynomial
 * at gamma using Horner.
 * Input: coeffs of pa in [0, 2^64[
 * Output: a in [0, p[
 */
void
from_PMNS (const felm_t pa, felm_t a)
{
  digit_t t[NWORDS_FIELD], q[1];

  /* set a to leading coefficient */
  t[0] = pa[NWORDS_FIELD-1];
  unsigned int nw = 1;

  for (unsigned int i = 1; i < NCOEFFS_FIELD; i++)
  {
    /* t <- t*gamma */
    t[nw] = mpn_mul_1 ((mp_ptr) t, (mp_srcptr) t, nw, PMNS_GAMMA);
    /* t <- t+pa[j] */
    t[nw] += mpn_add_1 ((mp_ptr) t, (mp_srcptr) t, nw, pa[NCOEFFS_FIELD-1-i]);
    if (t[nw])
      nw += 1;
  }

  mpn_tdiv_qr ((mp_ptr) q, (mp_ptr) t, 0, (mp_srcptr) t, nw, (mp_srcptr) p, NWORDS64_FIELD);

  for (unsigned int i = 0; i < nw; i++)
    a[i] = t[i];
  for (unsigned int i = nw; i < NWORDS_FIELD; i++)
    a[i] = 0;
}

//#define EXPERIMENTAL_FPADD
/* Modular addition, c = a+b mod p.
 * Inputs: coeffs of a and b in [0, 2^K[
 * Output: coeffs of c in [0, 2^K[
 */
__inline void
fpadd (const digit_t* pa, const digit_t* pb, digit_t* pc)
{
#ifndef EXPERIMENTAL_FPADD
  digit_t carry = 0;
#else
  digit_t idx_prev = 0;
#endif
  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
  {
    pc[i] = pa[i] + pb[i];
    digit_t idx = pc[i] >> (PMNS_K-1); /* idx is in [0, 4[ */
    pc[i] &= (PMNS_K_mask >> 1); /* modulo 2^(K-1) */
#ifndef EXPERIMENTAL_FPADD
    pc[i] += carry + pmns_corrections[idx];
    carry = pmns_carries[idx];
#else
    pc[i] += pmns_corrections_with_carries[(idx << 2) + idx_prev];
    idx_prev = idx;
#endif
  }
  /* propagate last carry to pc[0], we know this cannot generate another
   * carry.
   */
#ifndef EXPERIMENTAL_FPADD
  pc[0] += PMNS_E*carry;
#else
  pc[0] += PMNS_E*pmns_corrections_with_carries[idx_prev];
#endif
}

/* Modular subtraction, c = a-b mod p.
 * Inputs: coeffs of a and b in [0, 2^K[
 * Output: coeffs of c in [0, 2^K[
 */
__inline void
fpsub (const digit_t* pa, const digit_t* pb, digit_t* pc)
{
  pc[0] = pa[0] + PMNS_SUB_CST0 - pb[0];

  digit_t idx = pc[0] >> (PMNS_K-1); /* idx is in [0, 8[ */
  pc[0] &= (PMNS_K_mask >> 1); /* modulo 2^(K-1) */
  pc[0] += pmns_corrections[idx];
  digit_t carry = pmns_carries[idx];

  for (unsigned int i = 1; i < NCOEFFS_FIELD; i++)
  {
    pc[i] = pa[i] + PMNS_SUB_CST1 - pb[i];
    digit_t idx = pc[i] >> (PMNS_K-1); /* idx is in [0, 8[ */
    pc[i] &= (PMNS_K_mask >> 1); /* modulo 2^(K-1) */
    pc[i] += carry + pmns_corrections[idx];
    carry = pmns_carries[idx];
  }

  /* propagate last carry to pc[0], we know this cannot generate another
   * carry.
   */
  pc[0] += PMNS_E*carry;
}

/* Modular negation, a = -a mod p.
 * Input/output: coeffs of a in [0, 2^K[
 */
__inline void
fpneg (digit_t* pa)
{
  pa[0] = PMNS_SUB_CST0 - pa[0]; /* now pa[0] is in ]0, 2^K+gamma[ */
  digit_t carry = pa[0] > PMNS_K_MINUS_EP1; /* 2^K-(e+1) */
  pa[0] -= carry ? PMNS_GAMMA : 0;
  for (unsigned int i = 1; i < NCOEFFS_FIELD; i++)
  {
    digit_t tmp = carry ? PMNS_SUB_CST1P1 : PMNS_SUB_CST1 ;
    pa[i] = tmp - pa[i]; /* now pa[i] is in ]0, 2^K+gamma[ */
    carry = pa[i] & (1UL << PMNS_K); /* test if >= 2^K */
    pa[i] -= carry ? PMNS_GAMMA : 0;
  }

  /* propagate last carry to pa[0], we know this cannot generate another
   * carry
   */
  pa[0] += carry ? PMNS_E : 0;
}


/* Modular division by two, c = a/2 mod p.
 * Input : coeffs of a in [0, 2^K[
 * Output: coeffs of c in [0, 2^K[
 * This function assumes that gamma is even.
 */
void fpdiv2 (const digit_t* pa, digit_t* pc)
{
  digit_t carry = 0;
  for (unsigned int i = NCOEFFS_FIELD; i--; )
  {
    pc[i] = pa[i] + carry;
    int odd = pc[i] & 1;
    pc[i] >>= 1; /* = (pc[i]-1)/2  or pc[i]/2 */
    carry = odd ? PMNS_GAMMA : 0;
  }

  /* propagate last carry to pa[NCOEFFS_FIELD-1], we need to divide it by e,
   * and again by 2 because pa[NCOEFFS_FIELD-1] was already divided by 2. This
   * addition cannot generate another carry
   * So, if there is a carry, add gamma/(2*e).
   */
  pc[NCOEFFS_FIELD-1] += carry ? PMNS_GAMMA_OVER_2E : 0;
}

/* fpcorrection is a no-op in our case */
void
fpcorrection (digit_t* pa)
{
}

/* PMNS addition without coeffs reduction
 * The last parameter is not taken into account and is assumed to be equal to
 * NCOEFFS_FIELD
 */
__inline unsigned int
mp_add (const digit_t* pa, const digit_t* pb, digit_t* pc, const unsigned int _)
{
  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
    pc[i] = pa[i] + pb[i];
  return 0;
}

/* Utility function for mp_mul and rdc_mont */
static __inline void
coeff_x_coeff (const digit_t a, const digit_t b, digit_t* c)
{
  *((uint128_t *)c) = (uint128_t)a * (uint128_t)b;
}

#ifndef _NO_MP_MUL
/* Quadratic polynomial multiplication in Z[X]
 * pc = pa * pb with deg pa = deg pb = ncoeffs-1, deg pc = 2*ncoeffs-2
 * Utility function for mp_mul.
 */
static __inline void
mp_mul_basecase (const digit_t *pa, const digit_t *pb, digit_t *pc,
                                                    const unsigned int ncoeffs)
{
  for (unsigned int i = 0; i < 4*ncoeffs-2; i++)
    pc[i] = 0;

  for (unsigned int i = 0; i < ncoeffs; i++)
    for (unsigned int j = 0; j < ncoeffs; j++)
    {
      digit_t carry;
      digit_t t[2];
      unsigned int idx = 2*(i+j);

      coeff_x_coeff (pa[i], pb[j], t);
      ADD (pc[idx], t[0], carry, pc[idx]); /* no input carry */
      pc[idx+1] += t[1] + carry; /* no output carry */
    }
}

/* Modular addition, c = a+c mod p.
 * Inputs: coeffs of a and c fit in two words
 * Output: coeffs of c fit in two words
 * Utility function for mp_mul.
 */
static __inline void
mp_addinx2_pmns (const digit_t* a, digit_t* c, unsigned int ncoeffs)
{
  for (unsigned int i = 0; i < ncoeffs; i++)
  {
    digit_t carry;
    ADD (c[2*i], a[2*i], carry, c[2*i]); /* no input carry */
    c[2*i+1] += a[2*i+1] + carry; /* no output carry */
  }
}

/* Multiplication, c = a*b mod E.
 * Inputs: coeffs of pa and pb in [0, 2^(K+1)[
 * Output: coeffs of pc in [0, n*e*2^(2K+2)[
 * The last parameter is not taken into account and is assumed to be equal to
 * NCOEFFS_FIELD
 */
void
mp_mul (const digit_t* pa, const digit_t* pb, digit_t* pc, const unsigned int _)
{
#ifdef KARATSUBA
  #if NCOEFFS_FIELD % 2 == 1
    #error "Karatsuba only works with even NCOEFFS_FIELD"
  #endif

  digit_t t0[NCOEFFS_FIELD/2], t1[NCOEFFS_FIELD/2];
  digit_t tt1[2*NCOEFFS_FIELD-1], tt2[2*NCOEFFS_FIELD-1];

  unsigned int m = NCOEFFS_FIELD/2;

  /* Implicit splitting: pa = a0 + a1*x^m, pb = b0 + b1*x^m
   * pc  = a0 * b0
   * tt2 = a1 * b1
   */

  mp_mul_basecase (pa, pb, pc, m);
  mp_mul_basecase (pa+m, pb+m, tt2, m);

  /* t0 = (a0 + a1), t1 = (b0 + b1) */
  for (unsigned int i = 0; i < m; i++)
  {
    t0[i] = pa[i] + pa[i+m];
    t1[i] = pb[i] + pb[i+m];
  }

  /* tt1 = (a0 + a1)*(b0 + b1) */
  mp_mul_basecase (t0, t1, tt1, m);

  /* tt1 = tt1 - pc - tt2 */
  mp_dblsubx2_pmns (pc, tt2, tt1);

  /* pc[i] for i < 2*(NCOEFFS_FIELD-1) was set by first mul (a0*b0) */
  pc[2*NCOEFFS_FIELD-2] = 0;
  pc[2*NCOEFFS_FIELD-1] = 0;

  /* Add-128 tt1[0...2m-2] to pc[2m...]
   * reduced mod E for coeffs of tt1 of degree >= 2m
   */
  mp_addinx2_pmns (tt1, pc+2*m, m);
  for (unsigned j=0; j < PMNS_E; j++)
    mp_addinx2_pmns (tt1+2*m, pc, m-1);

  /* Add-128 tt2[0...2m-2] to pc[0...]
   * reduced mod E
   */
  for (unsigned int j = 0; j < PMNS_E; j++)
    mp_addinx2_pmns (tt2, pc, 2*m-1);

#else
  /* naive (quadratic) multiplication: pa * pb mod E */
  unsigned int carry = 0;

  /* Init and set to zero output polynomial pc */
  for (unsigned int i = 0; i < 2*NCOEFFS_FIELD; i++)
    pc[i] = 0;

  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
  {
    for (unsigned int j = 0; j < NCOEFFS_FIELD; j++)
    {
      digit_t t[2];
      coeff_x_coeff (pa[i], pb[j], t);
      unsigned int k = i+j;
      if (k < NCOEFFS_FIELD)
      {
        unsigned int idx = 2*k;
        ADD (pc[idx], t[0], carry, pc[idx]); /* no input carry */
        pc[idx+1] += t[1] + carry; /* no output carry */
      }
      else
      {
        /* Add pc_high to pc_low 'e' times */
        for (unsigned i = 0 ; i < PMNS_E ; i++)
        {
          unsigned int idx = 2*k-2*NCOEFFS_FIELD;
          ADD (pc[idx], t[0], carry, pc[idx]); /* no input carry */
          pc[idx+1] += t[1] + carry; /* no output carry */
        }
      }
    }
  }
#endif
}
#endif

#ifndef _NO_RDC_MONT
/* Montgomery reduction: mc = ma*R^-1 mod E, where R = 2^64.
 * Input: coeffs of ma in [0, 2^(K-1+64)[
 * Output: coeffs of mc in [0, 2^K[
 */
void
rdc_mont (digit_t* ma, digit_t* mc)
{
    /* Montgomery reduction:
   * Q = ma * Mp mod E mod 2^64
   * Both ma and Mp can be taken mod 2^64
   */
  digit_t q;

  /*        /  p[n-1] + gamma/e*p[0] + gamma^2/e*p[1] modulo 2^64,  for i = n-1
   * Q[i] = {  p[n-2] + gamma*p[n-1] + gamma^2/e*p[0] modulo 2^64,  for i = n-2
   *        \  p[i]   + gamma*p[i+1] + gamma^2*p[i+1] modulo 2^64,  for i <= n-3
   *
   * In particular: Q[i] = gamma*Q[i+1] + p[i] modulo 2^64,  for 0 <= i <= n-2
   */
  q = ma[(NCOEFFS_FIELD-1) << 1];
  q += ma[0] * PMNS_GAMMA_OVER_E;
  q += ma[2] * PMNS_GAMMA2_OVER_E_MOD;

  for (unsigned int i = NCOEFFS_FIELD-1; i--; )
  {
    digit_t t[2];
    digit_t carry;
    unsigned int idx = i << 1; /* 2*i */
    digit_t tmp = ma[idx];
    /* mul Q[i+1 modulo NCOEFFS_FIELD] by gamma/E or gamma */
    coeff_x_coeff (q, PMNS_GAMMA, t);
    /* tmp+t[0] = ma[idx]+PMNS_GAMMA*q is the value of q for the next iter */
    ADD (tmp, t[0], carry, q);
    mc[i] = ma[idx+1] + t[1] + carry; /* no output carry */
    /* Note: we should substract Q[i] but subtraction can be ommited since we
     * know that the result on the least significant word is zero. In
     * particular, it implies that a borrow cannot happen, so we do not need the
     * subtraction for most significant word either. */
#if 0
    digit_t borrow;
    SUB (tmp, Q[i], borrow, tmp); /* no input borrow */
    mc[i] -= borrow; /* no output borrow */
#endif
  }

  { /* for i = NCOEFFS_FIELD-1 */
    digit_t t[2];
    digit_t carry;
    unsigned int idx = (NCOEFFS_FIELD-1) << 1; /* 2*i */
    digit_t tmp = ma[idx];
    coeff_x_coeff (q, PMNS_GAMMA_OVER_E, t);
    ADD (tmp, t[0], carry, tmp); /* XXX here we just need the carry */
    mc[NCOEFFS_FIELD-1] = ma[idx+1] + t[1] + carry; /* no output carry */
  }

}
#endif

#ifndef _NO_MP_DBLSUBx2_PMNS
/* Double modular subtraction, c = c-a-b mod p.
 * Inputs: coeffs of a, b and c in [0, n*e*2^(2*K)[
 * Output: coeffs of c in [0, n*e*2^(2*K+2)[
 * Assumption: the output coeffs of c are positive. If this is not the case, do
 * not use this function.
 */
__inline void
mp_dblsubx2_pmns (const digit_t* a, const digit_t* b, digit_t* c)
{
  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
  {
    unsigned int borrow;
    SUB (c[2*i], a[2*i], borrow, c[2*i]); /* no input borrow */
    c[2*i+1] -= a[2*i+1]+borrow; /* no output borrow */

    SUB (c[2*i], b[2*i], borrow, c[2*i]); /* no input borrow */
    c[2*i+1] -= b[2*i+1]+borrow; /* no output borrow */
  }
}
#endif

#ifndef _NO_MP_SUBx2_PMNS
/* Modular subtraction, c = a-b mod p.
 * Inputs: coeffs of a and b in [0, n*e*2^(2*K)[
 * Output: coeffs of c in [0, n*e*2^(2*K+1)+something[
 */
__inline void
mp_subx2_pmns (const digit_t* a, const digit_t* b, digit_t* c)
{
  digit_t carry, borrow;
  ADD (a[0], PMNS_SUBx2_CST0_0, carry, c[0]); /* no input carry */
  c[1] = a[1] + PMNS_SUBx2_CST0_1 + carry; /* no output carry */

  SUB (c[0], b[0], borrow, c[0]); /* no input borrow */
  c[1] -= b[1]+borrow; /* no output borrow */

  for (unsigned int i = 1; i < NCOEFFS_FIELD; i++)
  {
    ADD (a[2*i], PMNS_SUBx2_CST1_0, carry, c[2*i]); /* no input carry */
    c[2*i+1] = a[2*i+1] + PMNS_SUBx2_CST1_1 + carry; /* no output carry */

    SUB (c[2*i], b[2*i], borrow, c[2*i]); /* no input borrow */
    c[2*i+1] -= b[2*i+1]+borrow; /* no output borrow */
  }
}
#endif
