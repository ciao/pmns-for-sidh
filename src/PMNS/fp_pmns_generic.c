#include <gmp.h>

#include "pmns_arith.h"

extern const uint64_t p[NWORDS64_FIELD];

/* Conversion from a residue to PMNS representation by writing v in basis gamma
 * with digit in [0, gamma[.
 * Input: a in [0, p[
 * Output: coeffs of pa in [0, gamma[
 */
void
to_PMNS (const felm_t a, felm_t pa)
{
  digit_t c[NWORDS_FIELD] = {0}, q[NWORDS_FIELD];
  fpcopy (a, c);

  /* */
  unsigned nw = NWORDS_FIELD;
  for ( ; nw > 0 && c[nw-1] == 0; nw--);

  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
  {
    if (nw >= NWORDS_COEFF)
    {
      mpn_tdiv_qr ((mp_ptr) q, (mp_ptr) pa+NWORDS_COEFF*i, 0, (mp_ptr) c, nw, (mp_ptr) PMNS_GAMMA, NWORDS_COEFF);
      if (nw > NWORDS_COEFF-1)
        nw -= NWORDS_COEFF-1;
      else
        nw = 0;
      for ( ; nw > 0 && q[nw-1] == 0; nw--);
      mpn_copyi ((mp_ptr) c, (mp_ptr) q, nw);
    }
    else
    {
      mpn_copyi ((mp_ptr) pa+NWORDS_COEFF*i, (mp_ptr) c, NWORDS_COEFF);
      mpn_zero ((mp_ptr) c, NWORDS_COEFF);
    }
  }
}

/* Conversion from a PMNS polynomial to a residue by evaluating the polynomial
 * at gamma using Horner.
 * Input: coeffs of pa in [0, 2^64[
 * Output: a in [0, p[
 */
void
from_PMNS (const felm_t pa, felm_t a)
{
  digit_t t[NWORDS_FIELD], r[NWORDS_FIELD], q[1];

  /* set a to leading coefficient */
  mpn_copyi ((mp_ptr) t, (mp_ptr) pa+NWORDS_FIELD-NWORDS_COEFF, NWORDS_COEFF);
  unsigned int cy, nw = NWORDS_COEFF;

  for (unsigned int i = 1; i < NCOEFFS_FIELD; i++)
  {
    /* t <- t*gamma */
    mpn_mul ((mp_ptr) r, (mp_ptr) t, nw, (mp_ptr) PMNS_GAMMA, NWORDS_COEFF);
    nw += NWORDS_COEFF;
    /* t <- t+pa[i] */
    cy = mpn_add ((mp_ptr) t, (mp_ptr) r, nw, (mp_ptr) pa+NWORDS_FIELD-(i+1)*NWORDS_COEFF, NWORDS_COEFF);
    if (cy)
    {
      t[nw] = 1;
      nw++;
    }
    else
      for ( ; nw > NWORDS_COEFF && t[nw-1] == 0; nw--);
  }

  for ( ; nw > 0 && t[nw-1] == 0; nw--);
  if (nw >= NWORDS64_FIELD)
    mpn_tdiv_qr ((mp_ptr) q, (mp_ptr) t, 0, (mp_srcptr) t, nw, (mp_srcptr) p, NWORDS64_FIELD);

  for (unsigned int i = 0; i < nw; i++)
    a[i] = t[i];
  for (unsigned int i = nw; i < NWORDS_FIELD; i++)
    a[i] = 0;
}

#ifndef _NO_PMNS_COEFF_REDUC
__inline void
pmns_coeff_reduc (digit_t *p)
{
  digit_t carry = 0;
  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
  {
    unsigned int offset = i*NWORDS_COEFF;
    digit_t idx = p[offset+NWORDS_COEFF-1] >> (PMNS_K_last_digit_shift-1); /* idx is in [0, 4[ */
    p[offset+NWORDS_COEFF-1] &= (PMNS_K_last_digit_mask >> 1); /* modulo 2^(K-1) */
    mpn_add_1 ((mp_ptr) p+offset, (mp_srcptr) p+offset, NWORDS_COEFF, carry);
    mpn_add_n ((mp_ptr) p+offset, (mp_srcptr) p+offset, (mp_srcptr) pmns_corrections+idx*NWORDS_COEFF, NWORDS_COEFF);
    carry = pmns_carries[idx];
  }
  /* propagate last carry to pc[0], we know this cannot generate another
   * carry.
   */
  for (unsigned int j = 0; j < PMNS_E; j++)
    mpn_add_1 ((mp_ptr) p, (mp_srcptr)p, NWORDS_COEFF, carry);
}
#endif

#ifndef _NO_FPADD
/* Modular addition, c = a+b mod p.
 * Inputs: coeffs of a and b in [0, 2^K[
 * Output: coeffs of c in [0, 2^K[
 */
__inline void
fpadd (const digit_t* pa, const digit_t* pb, digit_t* pc)
{
  digit_t carry = 0;
  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
  {
    unsigned int offset = i*NWORDS_COEFF;
    mpn_add_n ((mp_ptr) pc+offset, (mp_srcptr) pa+offset, (mp_srcptr) pb+offset, NWORDS_COEFF);
    digit_t idx = pc[offset+NWORDS_COEFF-1] >> (PMNS_K_last_digit_shift-1); /* idx is in [0, 4[ */
    pc[offset+NWORDS_COEFF-1] &= (PMNS_K_last_digit_mask >> 1); /* modulo 2^(K-1) */
    mpn_add_1 ((mp_ptr) pc+offset, (mp_srcptr) pc+offset, NWORDS_COEFF, carry);
    mpn_add_n ((mp_ptr) pc+offset, (mp_srcptr) pc+offset, (mp_srcptr) pmns_corrections+idx*NWORDS_COEFF, NWORDS_COEFF);
    carry = pmns_carries[idx];
  }
  /* propagate last carry to pc[0], we know this cannot generate another
   * carry.
   */
  for (unsigned int j = 0; j < PMNS_E; j++)
    mpn_add_1 ((mp_ptr) pc, (mp_srcptr)pc, NWORDS_COEFF, carry);
}
#endif

#ifndef _NO_FPSUB
/* Modular subtraction, c = a-b mod p.
 * Inputs: coeffs of a and b in [0, 2^K[
 * Output: coeffs of c in [0, 2^K[
 */
__inline void
fpsub (const digit_t* pa, const digit_t* pb, digit_t* pc)
{
  digit_t tmp[NWORDS_COEFF];

  mpn_sub_n ((mp_ptr) tmp, (mp_srcptr) pmns_sub_cst0, (mp_srcptr) pb, NWORDS_COEFF);
  mpn_add_n ((mp_ptr) pc, (mp_srcptr) pa, (mp_srcptr) tmp, NWORDS_COEFF);

  digit_t idx = pc[NWORDS_COEFF-1] >> (PMNS_K_last_digit_shift-1); /* idx is in [0, 8[ */
  pc[NWORDS_COEFF-1] &= (PMNS_K_last_digit_mask >> 1); /* modulo 2^(K-1) */
  mpn_add_n ((mp_ptr) pc, (mp_srcptr) pc, (mp_srcptr) pmns_corrections+idx*NWORDS_COEFF, NWORDS_COEFF);
  digit_t carry = pmns_carries[idx];

  for (unsigned int i = 1; i < NCOEFFS_FIELD; i++)
  {
    unsigned int offset = i*NWORDS_COEFF;
    mpn_sub_n ((mp_ptr) tmp, (mp_srcptr) pmns_sub_cst1, (mp_srcptr) pb+offset, NWORDS_COEFF);
    mpn_add_n ((mp_ptr) pc+offset, (mp_srcptr) pa+offset, (mp_srcptr) tmp, NWORDS_COEFF);
    digit_t idx = pc[offset+NWORDS_COEFF-1] >> (PMNS_K_last_digit_shift-1); /* idx is in [0, 8[ */
    pc[offset+NWORDS_COEFF-1] &= (PMNS_K_last_digit_mask >> 1); /* modulo 2^(K-1) */
    mpn_add_1 ((mp_ptr) pc+offset, (mp_srcptr) pc+offset, NWORDS_COEFF, carry);
    mpn_add_n ((mp_ptr) pc+offset, (mp_srcptr) pc+offset, (mp_srcptr) pmns_corrections+idx*NWORDS_COEFF, NWORDS_COEFF);
    carry = pmns_carries[idx];
  }

  /* propagate last carry to pc[0], we know this cannot generate another
   * carry.
   */
  for (unsigned int j = 0; j < PMNS_E; j++)
    mpn_add_1 ((mp_ptr) pc, (mp_srcptr) pc, NWORDS_COEFF, carry);
}
#endif

#ifndef _NO_FPNEG
/* Modular negation, a = -a mod p.
 * Input/output: coeffs of a in [0, 2^K[
 */
__inline void
fpneg (digit_t* pa)
{
  mpn_sub_n ((mp_ptr) pa, (mp_srcptr) pmns_sub_cst0, (mp_srcptr) pa, NWORDS_COEFF);
  /* now the first coeff of pa is in ]0, 2^K+gamma[ */
  digit_t carry = mpn_cmp ((mp_srcptr) pa, (mp_srcptr) pmns_K_minus_ep1, NWORDS_COEFF) > 0;
  mpn_cnd_sub_n (carry, (mp_ptr) pa, (mp_srcptr) pa, (mp_srcptr) PMNS_GAMMA, NWORDS_COEFF);
  for (unsigned int i = 1; i < NCOEFFS_FIELD; i++)
  {
    unsigned int offset = i*NWORDS_COEFF;
    const digit_t *tmp = carry ? pmns_sub_cst1p1 : pmns_sub_cst1;
    mpn_sub_n ((mp_ptr) pa+offset, (mp_srcptr) tmp, (mp_srcptr) pa+offset, NWORDS_COEFF);
    /* now the ith coeff of pa is in ]0, 2^K+gamma[ */
    carry = pa[offset+NWORDS_COEFF-1] & (1UL << PMNS_K_last_digit_shift); /* test if >= 2^K */
    mpn_cnd_sub_n (carry, (mp_ptr) pa+offset, (mp_srcptr) pa+offset, (mp_srcptr) PMNS_GAMMA, NWORDS_COEFF);
  }

  /* propagate last carry to pa[0], we know this cannot generate another
   * carry
   */
  mpn_add_1 ((mp_ptr) pa, (mp_srcptr) pa, NWORDS_COEFF, carry ? PMNS_E : 0);
}
#endif


#ifndef _NO_FPDIV2
/* Modular division by two, c = a/2 mod p.
 * Input : coeffs of a in [0, 2^K[
 * Output: coeffs of c in [0, 2^K[
 * This function assumes that gamma is even.
 */
void fpdiv2 (const digit_t* pa, digit_t* pc)
{
  mp_limb_t odd = 0;
  for (unsigned int i = NCOEFFS_FIELD; i--; )
  {
    unsigned int offset = i*NWORDS_COEFF;
    mpn_cnd_add_n (odd, (mp_ptr) pc+offset, (mp_srcptr) pa+offset, (mp_srcptr) PMNS_GAMMA, NWORDS_COEFF);
    /* (pc[i]-1)/2  or pc[i]/2 */
    odd = mpn_rshift ((mp_ptr) pc+offset, (mp_srcptr) pc+offset, NWORDS_COEFF, 1);
  }

  /* propagate last carry to pa[NCOEFFS_FIELD-1], we need to divide it by e,
   * and again by 2 because pa[NCOEFFS_FIELD-1] was already divided by 2. This
   * addition cannot generate another carry
   * So, if there is a carry, add gamma/(2*e).
   */
  unsigned int offset = (NCOEFFS_FIELD-1)*NWORDS_COEFF;
  mpn_cnd_add_n (odd, (mp_ptr) pc+offset, (mp_srcptr) pc+offset, (mp_srcptr) PMNS_GAMMA_OVER_2E, NWORDS_COEFF);
}
#endif

/* fpcorrection is a no-op in our case */
void
fpcorrection (digit_t* pa)
{
}

#ifndef _NO_MP_ADD
/* PMNS addition without coeffs reduction
 * The last parameter is not taken into account and is assumed to be equal to
 * NCOEFFS_FIELD
 */
__inline unsigned int
mp_add (const digit_t* pa, const digit_t* pb, digit_t* pc, const unsigned int _)
{
  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
    mpn_add_n ((mp_ptr) pc+i*NWORDS_COEFF, (mp_srcptr) pa+i*NWORDS_COEFF, (mp_srcptr) pb+i*NWORDS_COEFF, NWORDS_COEFF);
  return 0;
}
#endif

#ifndef _NO_MP_MUL
/* Multiplication, c = a*b mod E.
 * Inputs: coeffs of pa and pb in [0, 2^(K+1)[
 * Output: coeffs of pc in [0, n*e*2^(2K+2)[
 * The last parameter is not taken into account and is assumed to be equal to
 * NCOEFFS_FIELD
 */
void
mp_mul (const digit_t* pa, const digit_t* pb, digit_t* pc, const unsigned int _)
{
  /* naive (quadratic) multiplication: pa * pb mod E */

  /* Init and set to zero output polynomial pc */
  for (unsigned int i = 0; i < 2*NWORDS_FIELD; i++)
    pc[i] = 0;

  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
  {
    for (unsigned int j = 0; j < NCOEFFS_FIELD; j++)
    {
      digit_t tmp[2*NWORDS_COEFF];
      mpn_mul_n ((mp_ptr) tmp, (mp_srcptr) pa+NWORDS_COEFF*i, (mp_srcptr) pb+NWORDS_COEFF*j, NWORDS_COEFF);
      unsigned int k = i+j;
      if (k < NCOEFFS_FIELD)
        mpn_add_n ((mp_ptr) pc+2*k*NWORDS_COEFF, (mp_srcptr) pc+2*k*NWORDS_COEFF, (mp_srcptr) tmp, 2*NWORDS_COEFF);
      else
      {
        k -= NCOEFFS_FIELD;
        for (unsigned n = 0 ; n < PMNS_E ; n++)
          mpn_add_n ((mp_ptr) pc+2*k*NWORDS_COEFF, (mp_srcptr) pc+2*k*NWORDS_COEFF, (mp_srcptr) tmp, 2*NWORDS_COEFF);
      }
    }
  }
}
#endif

#ifndef _NO_RDC_MONT
/* Montgomery reduction: mc = ma*R^-1 mod E, where R = 2^(64*NWORDS_COEFF).
 * Input: coeffs of ma in [0, 2^(K-1+64*NWORDS_COEFF)[
 * Output: coeffs of mc in [0, 2^K[
 */
void
rdc_mont (digit_t* ma, digit_t* mc)
{
  /* Montgomery reduction:
   * omega = 64*NWORDS_COEFF
   * Q = ma * Mp mod E mod 2^omega
   * Both ma and Mp can be taken mod 2^omega
   */
  digit_t q[NWORDS_COEFF], t[2*NWORDS_COEFF];

  /*        /  p[n-1] + gamma/e*p[0] + gamma^2/e*p[1] mod 2^omega, for i = n-1
   * Q[i] = {  p[n-2] + gamma*p[n-1] + gamma^2/e*p[0] mod 2^omega, for i = n-2
   *        \  p[i]   + gamma*p[i+1] + gamma^2*p[i+1] mod 2^omega, for i <= n-3
   *
   * In particular: Q[i] = gamma*Q[i+1] + p[i] modulo 2^omega, for 0 <= i <= n-2
   */
  mpn_copyi ((mp_ptr) q, (mp_srcptr) ma+2*(NWORDS_FIELD-NWORDS_COEFF), NWORDS_COEFF);
  mpn_mul_n ((mp_ptr) t, (mp_srcptr) ma, (mp_srcptr) PMNS_GAMMA_OVER_E, NWORDS_COEFF);
  mpn_add_n ((mp_ptr) q, (mp_srcptr) q, (mp_srcptr) t, NWORDS_COEFF);
  mpn_mul_n ((mp_ptr) t, (mp_srcptr) ma+2*NWORDS_COEFF, (mp_srcptr) PMNS_GAMMA2_OVER_E_MOD, NWORDS_COEFF);
  mpn_add_n ((mp_ptr) q, (mp_srcptr) q, (mp_srcptr) t, NWORDS_COEFF);

  for (unsigned int i = NCOEFFS_FIELD-1; i--; )
  {
    /* mul Q[i+1 modulo NWORDS_FIELD] by gamma/E or gamma */
    mpn_mul_n ((mp_ptr) t, (mp_srcptr) q, (mp_srcptr) PMNS_GAMMA, NWORDS_COEFF);
    /* tmp+t[0] = ma[idx]+PMNS_GAMMA*q is the value of q for the next iter */
    mpn_add_n ((mp_ptr) q, (mp_srcptr) t, (mp_srcptr) ma+(i << 1)*NWORDS_COEFF, 2*NWORDS_COEFF);
    mpn_copyi ((mp_ptr) mc+i*NWORDS_COEFF, (mp_srcptr) q+NWORDS_COEFF, NWORDS_COEFF);
    /* Note: we should substract Q[i] but subtraction can be ommited since we
     * know that the result on the least significant word is zero. In
     * particular, it implies that a borrow cannot happen, so we do not need the
     * subtraction for most significant word either. */
  }

  { /* for i = NWORDS_FIELD-1 */
    mpn_mul_n ((mp_ptr) t, (mp_srcptr) q, (mp_srcptr) PMNS_GAMMA_OVER_E, NWORDS_COEFF);

    mpn_add_n ((mp_ptr) q, (mp_srcptr) t, (mp_srcptr) ma+((NCOEFFS_FIELD-1) << 1)*NWORDS_COEFF, 2*NWORDS_COEFF);
    mpn_copyi ((mp_ptr) mc+NWORDS_FIELD-NWORDS_COEFF, (mp_srcptr) q+NWORDS_COEFF, NWORDS_COEFF);
  }
}
#endif

#ifndef _NO_MP_DBLSUBx2_PMNS
/* Double modular subtraction, c = c-a-b mod p.
 * Inputs: coeffs of a, b and c in [0, n*e*2^(2*K)[
 * Output: coeffs of c in [0, n*e*2^(2*K+2)[
 * Assumption: the output coeffs of c are positive. If this is not the case, do
 * not use this function.
 */
__inline void
mp_dblsubx2_pmns (const digit_t* a, const digit_t* b, digit_t* c)
{
  for (unsigned int i = 0; i < NCOEFFS_FIELD; i++)
  {
    mpn_sub_n ((mp_ptr) c+i*2*NWORDS_COEFF, (mp_srcptr) c+i*2*NWORDS_COEFF, (mp_srcptr) a+i*2*NWORDS_COEFF, 2*NWORDS_COEFF);
    mpn_sub_n ((mp_ptr) c+i*2*NWORDS_COEFF, (mp_srcptr) c+i*2*NWORDS_COEFF, (mp_srcptr) b+i*2*NWORDS_COEFF, 2*NWORDS_COEFF);
  }
}
#endif

#ifndef _NO_MP_SUBx2_PMNS
/* Modular subtraction, c = a-b mod p.
 * Inputs: coeffs of a and b in [0, n*e*2^(2*K)[
 * Output: coeffs of c in [0, n*e*2^(2*K+1)+something[
 */
__inline void
mp_subx2_pmns (const digit_t* a, const digit_t* b, digit_t* c)
{
  mpn_add_n ((mp_ptr) c, (mp_srcptr) a, (mp_srcptr) pmns_subx2_cst0, 2*NWORDS_COEFF);
  mpn_sub_n ((mp_ptr) c, (mp_srcptr) c, (mp_srcptr) b, 2*NWORDS_COEFF);

  for (unsigned int i = 1; i < NCOEFFS_FIELD; i++)
  {
    unsigned int offset = i*2*NWORDS_COEFF;
    mpn_add_n ((mp_ptr) c+offset, (mp_srcptr) a+offset, (mp_srcptr) pmns_subx2_cst1, 2*NWORDS_COEFF);
    mpn_sub_n ((mp_ptr) c+offset, (mp_srcptr) c+offset, (mp_srcptr) b+offset, 2*NWORDS_COEFF);
  }
}
#endif
