#include "../src/P736/P736_internal.h"

#define from_PMNS from_PMNS_736
#define to_PMNS to_PMNS_736
#define fpadd fpadd736
#define fpsub fpsub736
#define fpneg fpneg736
#define fpdiv2 fpdiv2_736
#define fpmul_mont fpmul736_mont
#define fpsqr_mont fpsqr736_mont
#define fpinv_mont fpinv736_mont
#define fp2add fp2add736
#define fp2sub fp2sub736
#define fp2neg fp2neg736
#define fp2div2 fp2div2_736
#define fp2mul_mont fp2mul736_mont
#define fp2sqr_mont fp2sqr736_mont
#define fp2inv_mont fp2inv736_mont
#define mp_dblsubx2_pmns mp_dblsub736x2_pmns
#define mp_subx2_pmns mp_sub736x2_pmns
#define p p736
#define fp2zero fp2zero736

extern const uint64_t p736[NWORDS64_FIELD];

#include "arith_ourtests.c"
