#include "../src/P751/P751_internal.h"

#define from_PMNS from_PMNS_751
#define to_PMNS to_PMNS_751
#define fpadd fpadd751
#define fpsub fpsub751
#define fpneg fpneg751
#define fpdiv2 fpdiv2_751
#define fpmul_mont fpmul751_mont
#define fpsqr_mont fpsqr751_mont
#define fpinv_mont fpinv751_mont
#define fp2add fp2add751
#define fp2sub fp2sub751
#define fp2neg fp2neg751
#define fp2div2 fp2div2_751
#define fp2mul_mont fp2mul751_mont
#define fp2sqr_mont fp2sqr751_mont
#define fp2inv_mont fp2inv751_mont
#define mp_dblsubx2_pmns mp_dblsub751x2_pmns
#define mp_subx2_pmns mp_sub751x2_pmns
#define p p751
#define fp2zero fp2zero751

extern const uint64_t p751[NWORDS64_FIELD];

#include "arith_ourtests.c"
