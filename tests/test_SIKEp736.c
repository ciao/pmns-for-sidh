/********************************************************************************************
* SIDH: an efficient supersingular isogeny cryptography library
*
* Abstract: benchmarking/testing isogeny-based key encapsulation mechanism SIKEp736
*********************************************************************************************/ 

#include <stdio.h>
#include <string.h>
#include "test_extras.h"
#include "../src/P736/P736_api.h"


#define SCHEME_NAME    "Ourp736"

#define crypto_kem_keypair            crypto_kem_keypair_SIKEp736
#define crypto_kem_enc                crypto_kem_enc_SIKEp736
#define crypto_kem_dec                crypto_kem_dec_SIKEp736

#include "test_sike.c"
